import { NgModule } from '@angular/core';
import { PostDetailsComponent } from './post-details/post-details.component';
import {PostsRouting} from './posts.routing';
import {SharedModule} from '../shared/shared.module';
import {GoogleMapsModule} from '@angular/google-maps';



@NgModule({
  declarations: [PostDetailsComponent],
    imports: [
        PostsRouting,
        SharedModule,
        GoogleMapsModule,
    ]
})
export class PostsModule {}
