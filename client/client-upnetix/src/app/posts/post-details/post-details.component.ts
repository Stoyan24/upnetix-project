import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {PostDetailsInterface} from '../../common/models/post-models/post-details.interface';
import {UserTokenInfo} from '../../common/models/UserTokenInfo';
import {Subscription} from 'rxjs';
import {AuthService} from '../../services/auth.service';
import {LocationInterface} from "../../common/models/location-models/location.inteface";

@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.scss']
})
export class PostDetailsComponent implements OnInit, OnDestroy {

  public loggedUser: UserTokenInfo;
  private loggedUserSubscription: Subscription;
  public post: PostDetailsInterface;
  public center: Partial<LocationInterface>;
  public markerOptions: google.maps.MarkerOptions;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly authService: AuthService,
  ) { }

  public ngOnInit(): void {
    this.route.data.subscribe((u) => {
      this.post = u.post;
      this.center = {
        lat: u.post.location.lat,
        lng: u.post.location.lng,
      };
      this.markerOptions = {
        animation: google.maps.Animation.DROP,
        label: u.post.location.formatted_address,
      };
    });
    this.loggedUserSubscription = this.authService.loggedUserData$.subscribe(
      (data: UserTokenInfo) => this.loggedUser = data
    );
  }

  public ngOnDestroy(): void {
    this.loggedUserSubscription.unsubscribe();
  }


}
