import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {PostDetailsComponent} from './post-details/post-details.component';
import {PostDetailsResolver} from '../services/resolvers/post-details.resolver';
import {AuthGuard} from '../core/guards/auth.guard';


const routes: Routes = [
  {path: '', redirectTo: '/not-found', pathMatch: 'full'},
  {
    path: ':postId',
    component: PostDetailsComponent,
    resolve: {post: PostDetailsResolver},
    canActivate: [AuthGuard],
    data: {state: 'details'}
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PostsRouting {
}
