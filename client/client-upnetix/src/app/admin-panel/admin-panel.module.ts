import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminViewComponent } from './admin-view/admin-view.component';
import {AdminPanelRouting} from './admin-panel.routing';
import {SharedModule} from '../shared/shared.module';
import {ReactiveFormsModule} from '@angular/forms';



@NgModule({
  declarations: [AdminViewComponent],
  imports: [
    CommonModule,
    AdminPanelRouting,
    SharedModule,
    ReactiveFormsModule,
  ]
})
export class AdminPanelModule { }
