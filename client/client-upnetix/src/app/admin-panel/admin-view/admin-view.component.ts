import {Component, OnInit} from '@angular/core';
import {map} from 'rxjs/operators';
import {UsersInterface} from '../../common/models/user-models/users.interface';
import {ActivatedRoute} from '@angular/router';
import {SocketService} from '../../services/socket.service';
import {animate, sequence, style, transition, trigger} from '@angular/animations';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UsersService} from '../../services/users.service';

@Component({
  selector: 'app-admin-view',
  templateUrl: './admin-view.component.html',
  styleUrls: ['./admin-view.component.scss'],
  animations: [
    trigger('usersDeleteAnimation', [
      transition(':leave', [
        style({height: '*'}),
        animate(250, style({height: 0, opacity: 0}))
      ]),
    ]),
    trigger('countAnimation', [
      transition(':increment', [
        style({opacity: 0, transform: 'translateY(-30%)'}),
        animate('1000ms ease', style({opacity: 1, transform: 'translateY(-2%)'})),
      ]),
      transition(':decrement', [
        animate('1000ms ease', style({transform: 'translateY(+20%)'})),
      ]),
      transition(':leave', [
        animate('1000ms', style({transform: 'rotateY(180deg)', opacity: 0}))
      ]),
    ]),
    trigger('icon', [
      transition(':increment', [
        sequence([
            animate('500ms ease-in', style({color: 'green', transform: 'rotate(30deg)'})),
            animate('500ms ease-in', style({color: 'green', transform: 'rotate(-30deg)'})),
            animate('1000ms ease'),
          ]
        )
      ]),
      transition(':decrement', [
        sequence([
            animate('500ms ease-in', style({color: 'red', transform: 'rotate(30deg)'})),
            animate('500ms ease-in', style({color: 'red', transform: 'rotate(-30deg)'})),
            animate('1000ms ease'),
          ]
        )
      ]),
      transition(':leave', [
        animate('1000ms', style({transform: 'rotateY(180deg)', opacity: 0}))
      ]),
    ]),
  ]
})
export class AdminViewComponent implements OnInit {

  public users: UsersInterface[];
  public onlineUsersCount = 0;
  public globalMsgForm: FormGroup;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly socketService: SocketService,
    private readonly fb: FormBuilder,
    private readonly usersService: UsersService,
  ) {
  }

  ngOnInit(): void {
    this.globalMsgForm = this.fb.group({
      msg: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
    });
    this.route.data.pipe(
      map(u => u.users)
    ).subscribe(u => this.users = u);
    this.socketService.usersNumber()
      .subscribe((n) => this.onlineUsersCount = n);
  }

  public goGlobal(): void {
    this.socketService.sendGlobalMsg(this.globalMsgForm.value.msg);
    this.globalMsgForm.reset();
  }

  public deleteUser(userId): void {
    this.usersService.deleteUser(userId).subscribe(id => this.users = this.users.filter(u => u.userId !== id.userId,
      (e) => console.log(e)));
  }
}
