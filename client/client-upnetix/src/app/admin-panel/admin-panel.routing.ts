import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AdminViewComponent} from './admin-view/admin-view.component';
import {AllUsersResolver} from '../services/resolvers/all-users.resolver';

const routes: Routes = [
  {
    path: '',
    component: AdminViewComponent,
    resolve: {users: AllUsersResolver},
    data: {state: 'admin'}
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminPanelRouting {
}
