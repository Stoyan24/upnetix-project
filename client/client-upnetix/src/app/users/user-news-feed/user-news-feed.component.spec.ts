import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserNewsFeedComponent } from './user-news-feed.component';

describe('UserNewsFeedComponent', () => {
  let component: UserNewsFeedComponent;
  let fixture: ComponentFixture<UserNewsFeedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserNewsFeedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserNewsFeedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
