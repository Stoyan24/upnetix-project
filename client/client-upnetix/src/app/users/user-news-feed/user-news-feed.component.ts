import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {AllPostsInterface} from '../../common/models/post-models/all-posts.interface';
import {ActivatedRoute} from '@angular/router';
import {catchError, delay, finalize, map, tap} from 'rxjs/operators';
import {UsersService} from '../../services/users.service';
import {UserTokenInfo} from '../../common/models/UserTokenInfo';
import {AuthService} from '../../services/auth.service';
import {UsersInterface} from '../../common/models/user-models/users.interface';
import {PostsService} from '../../services/posts.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {SocketService} from '../../services/socket.service';

@Component({
  selector: 'app-user-news-feed',
  templateUrl: './user-news-feed.component.html',
  styleUrls: ['./user-news-feed.component.scss'],
})
export class UserNewsFeedComponent implements OnInit, OnDestroy {

  public skip = 9;
  public loggedUser: UserTokenInfo;
  private loggedUserSubscription: Subscription;
  public posts: AllPostsInterface[];
  public newPosts: AllPostsInterface[] = [];
  public commentBoxTrigger: boolean | string = false;
  public alertNotify = true;
  public disabler = false;


  public subscribers: UsersInterface[];
  public subscriptions: UsersInterface[];

  constructor(
    private readonly route: ActivatedRoute,
    private readonly usersService: UsersService,
    private readonly authService: AuthService,
    private readonly postsService: PostsService,
    private spinner: NgxSpinnerService,
    private readonly socketService: SocketService,
  ) {
  }

  public ngOnInit(): void {
    this.route.data.pipe(map(posts => posts.posts)).subscribe((p) => this.posts = p);
    this.loggedUserSubscription = this.authService.loggedUserData$.subscribe(
      (data: UserTokenInfo) => this.loggedUser = data
    );
    this.socketService.getNewPost().subscribe((p: AllPostsInterface) => {
      this.alertNotify = false;
      setTimeout(() => this.alertNotify = true, 3000);
      this.newPosts.unshift(p);
    });
  }

  public getConnections(type: string): void {
    this.usersService.getUserConnections(this.loggedUser.userId, type).subscribe(
      (c: UsersInterface[]) => {
        if (type === 'subscriptions') {
          this.subscriptions = c;
        } else if (type === 'subscribers') {
          this.subscribers = c;
        }
      }, (error) => console.log(error)
    );
  }

  public onScroll(): void {
    this.postsService.getAllPublicPosts(this.skip, 9)
      .pipe(
        tap(() => {
          this.spinner.show('load-more-spinner');
          this.disabler = true;
        }),
        delay(2000),
        tap((p) => this.posts.push(...p)),
        finalize(() => this.spinner.hide('load-more-spinner')),
        catchError(e => e)
      )
      .subscribe(() => {
        this.skip += 9;
        this.disabler = false;
      });
  }

  public unsubscribe(userId: string): void {
    this.usersService.connectionsAction(userId).subscribe((a) => {
        this.subscriptions = this.subscriptions.filter((u: UsersInterface) => u.userId !== userId);
      },
      (error) => console.log(error));
  }

  public showCommentTrigger(postId: string): void {
    this.commentBoxTrigger = postId;
  }

  public closeCommentBox(): void {
    this.commentBoxTrigger = false;
  }

  public ngOnDestroy(): void {
    this.loggedUserSubscription.unsubscribe();
  }

  public goToTop(bool): void {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
    this.alertNotify = bool;
  }
}
