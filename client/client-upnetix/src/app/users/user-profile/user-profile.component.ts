import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserTokenInfo} from '../../common/models/UserTokenInfo';
import {Subscription} from 'rxjs';
import {AllPostsInterface} from '../../common/models/post-models/all-posts.interface';
import {UsersInterface} from '../../common/models/user-models/users.interface';
import {ActivatedRoute} from '@angular/router';
import {UsersService} from '../../services/users.service';
import {AuthService} from '../../services/auth.service';
import {UserProfileInterface} from '../../common/models/user-models/user-profile.interface';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit, OnDestroy {

  public loggedUser: UserTokenInfo;
  private loggedUserSubscription: Subscription;
  public postsPublic: AllPostsInterface[] = [];
  public postsPrivate: AllPostsInterface[] = [];

  public likedPosts: AllPostsInterface[] = [];
  public currentUser: UserProfileInterface;

  public active;
  public buttonBool = false;


  public subscribers: UsersInterface[] = [];
  public subscriptions: UsersInterface[] = [];

  constructor(
    private readonly route: ActivatedRoute,
    private readonly usersService: UsersService,
    private readonly authService: AuthService,
  ) {
  }

  ngOnInit(): void {
    this.loggedUserSubscription = this.authService.loggedUserData$.subscribe(
      (data: UserTokenInfo) => this.loggedUser = data
    );
    this.route.data.subscribe((u) => {
      this.currentUser = u.user;
      this.load();
      this.emptier();
      this.buttonBool = this.currentUser.userId === this.loggedUser.userId;
    });
  }

  public emptier(): void {
    this.postsPrivate = [];
    this.likedPosts = [];
    this.subscribers = [];
    this.subscriptions = [];
  }

  public ngOnDestroy(): void {
    this.loggedUserSubscription.unsubscribe();
  }

  public connectionsActionA(userId: string): void {
    this.usersService.connectionsAction(userId).subscribe((a) => {
        this.currentUser.subscribers.push(this.loggedUser);
      },
      (error) => console.log(error));
  }

  public connectionsActionB(userId: string): void {
    this.usersService.connectionsAction(userId).subscribe((a) => {
        this.currentUser.subscribers = this.currentUser.subscribers.filter((u) => u.userId === userId);
      },
      (error) => console.log(error));
  }

  public checkSub(): boolean {
    return !this.currentUser.subscribers.some((e) => e.userId === this.loggedUser.userId);
  }

  public disable(): boolean {
    return this.checkSub() ? this.loggedUser.userId !== this.currentUser.userId : false;
  }

  public load(selectedNav: string = 'public'): void {
    switch (selectedNav) {
      case 'public':
        this.usersService.getUserPost(this.currentUser.userId, selectedNav).subscribe((p) => {
          this.postsPublic = p;
        });
        this.active = 'public';
        break;
      case 'private':
        this.usersService.getUserPost(this.currentUser.userId, selectedNav).subscribe((p) => {
          this.postsPrivate = p;
        });
        break;
      case 'liked':
        this.usersService.getUserLikedPosts(this.currentUser.userId).subscribe(
          (p: AllPostsInterface[]) => {
            this.likedPosts = p;
          }, (error) => console.log(error)
        );
        break;
      case 'subscribers':
        this.usersService.getUserConnections(this.currentUser.userId, selectedNav).subscribe(
          (c: UsersInterface[]) => {
            this.subscribers = c;
          }, (error) => console.log(error)
        );
        break;
      case 'subscriptions':
        this.usersService.getUserConnections(this.currentUser.userId, selectedNav).subscribe(
          (c: UsersInterface[]) => {
            this.subscriptions = c;
          }, (error) => console.log(error)
        );
        break;
    }
  }
}
