import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {UserNewsFeedComponent} from './user-news-feed/user-news-feed.component';
import {UserProfileComponent} from './user-profile/user-profile.component';
import {AllPubicPostsResolver} from '../services/resolvers/all-pubic-posts.resolver';
import {UserInfoResolver} from '../services/resolvers/user-info.resolver';
import {AuthGuard} from '../core/guards/auth.guard';

const routes: Routes = [
  {path: '', redirectTo: '/not-found', pathMatch: 'full', canActivate: [AuthGuard]},
  {
    path: ':username/news_feed',
    component: UserNewsFeedComponent,
    resolve: {posts: AllPubicPostsResolver},
    canActivate: [AuthGuard],
    data: {state: 'users'}
  },
  {
    path: 'profile/:userId',
    component: UserProfileComponent,
    resolve: {user: UserInfoResolver},
    canActivate: [AuthGuard],
    data: {state: 'profile'}
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRouting {
}
