import { NgModule } from '@angular/core';
import { UserNewsFeedComponent } from './user-news-feed/user-news-feed.component';
import {UsersRouting} from './users.routing';
import {SharedModule} from '../shared/shared.module';
import { UserProfileComponent } from './user-profile/user-profile.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';



@NgModule({
  declarations: [UserNewsFeedComponent, UserProfileComponent],
    imports: [
        UsersRouting,
        SharedModule,
        NgbModule,
    ]
})
export class UsersModule { }
