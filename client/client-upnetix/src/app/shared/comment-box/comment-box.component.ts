import {Component, Input, OnInit} from '@angular/core';
import {PostsService} from '../../services/posts.service';
import {CommentInterface} from '../../common/models/comment-models/comment.interface';
import {delay, finalize, map} from 'rxjs/operators';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CommentsService} from '../../services/comments.service';
import {UsersInterface} from '../../common/models/user-models/users.interface';
import {animate, style, transition, trigger} from '@angular/animations';
import {SocketService} from '../../services/socket.service';

@Component({
  selector: 'app-comment-box',
  templateUrl: './comment-box.component.html',
  styleUrls: ['./comment-box.component.scss'],
  animations: [
    trigger('commentAnimation', [
      transition(':leave', [
        style({height: '*'}),
        animate(250, style({height: 0, opacity: 0}))
      ]),
      transition(':enter', [
        style({transform: 'translateX(-100%)', opacity: 0}),
        animate('600ms')
      ])
    ])
  ]
})
export class CommentBoxComponent implements OnInit {

  @Input() postId: string;
  @Input() userInfo: UsersInterface;
  public spinner = true;
  public commentForm: FormGroup;
  public comments: CommentInterface[];

  constructor(
    private readonly postsService: PostsService,
    private readonly fb: FormBuilder,
    private readonly commentsService: CommentsService,
    private readonly socketService: SocketService,
  ) {
  }

  ngOnInit(): void {
    this.postsService.getPostComments(this.postId).pipe(
      delay(300),
      map((c) => this.comments = c),
      finalize(() => this.spinner = false)).subscribe();
    this.commentForm = this.fb.group({
      commentContent: ['', [Validators.required, Validators.minLength(15), Validators.maxLength(400)]]
    });
    this.socketService.getNewComments(this.postId).subscribe((c: CommentInterface) => {
      this.comments.push(c);
    });
  }

  public addComment(): void {
    this.commentsService.createComment(this.postId, this.commentForm.value).subscribe((c: CommentInterface) => {
        this.comments.push(c);
        this.socketService.emitNewComment({data: c, namespace: this.postId});
      },
      (e) => console.log(e),
      () => this.commentForm.reset());
  }

  public removeComment(commentId: string): void {
    this.commentsService.removeComment(commentId).subscribe((id) => {
        this.comments = this.comments.filter((c) => c.commentId !== id.commentId);
      },
      (e) => console.log(e),
    );
  }

}
