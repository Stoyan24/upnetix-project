import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NotificationInterface} from '../../common/models/notification-models/notification.interface';

@Component({
  selector: 'app-notification-box',
  templateUrl: './notification-box.component.html',
  styleUrls: ['./notification-box.component.scss']
})
export class NotificationBoxComponent implements OnInit {

  @Input() index: number;
  @Input() notification: NotificationInterface;
  @Output() notificationId = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }

  public readNotification(): void {
    if (!this.notification.read){
      this.notificationId.emit(this.notification.notificationId);
    }
  }
}
