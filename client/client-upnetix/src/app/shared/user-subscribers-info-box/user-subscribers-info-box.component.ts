import {Component, Input, OnInit} from '@angular/core';
import {animate, keyframes, state, style, transition, trigger} from '@angular/animations';
import {UsersInterface} from '../../common/models/user-models/users.interface';

@Component({
  selector: 'app-user-subscribers-info-box',
  templateUrl: './user-subscribers-info-box.component.html',
  styleUrls: ['./user-subscribers-info-box.component.scss'],
  // animations: [
  //   trigger('appear', [
  //     transition(':enter', [
  //       style({opacity: 0, transform: 'scale(0.3)'}),
  //       animate(1200, style({opacity: 1, transform: 'scale(1)'}))
  //     ])
  //   ])
  // ]
})
export class UserSubscribersInfoBoxComponent implements OnInit {

  @Input() user: UsersInterface;

  constructor() {
  }

  ngOnInit(): void {
  }

}
