import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserSubscribersInfoBoxComponent } from './user-subscribers-info-box.component';

describe('UserInfoBoxComponent', () => {
  let component: UserSubscribersInfoBoxComponent;
  let fixture: ComponentFixture<UserSubscribersInfoBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserSubscribersInfoBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserSubscribersInfoBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
