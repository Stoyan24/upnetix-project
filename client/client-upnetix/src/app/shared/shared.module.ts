import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PostViewUnauthorizedComponent} from './post-view-unauthorized/post-view-unauthorized.component';
import {ScrollTopButtonComponent} from './scroll-top-button/scroll-top-button.component';
import {ConnectionsTabComponent} from './connections-tab/connections-tab.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {UserSubscribersInfoBoxComponent} from './user-subscribers-info-box/user-subscribers-info-box.component';
import {PostViewComponent} from './post-view/post-view.component';
import {CommentBoxComponent} from './comment-box/comment-box.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {UserSubscriptionsInfoBoxComponent} from './user-subscriptions-info-box/user-subscriptions-info-box.component';
import {RouterModule} from '@angular/router';
import {ProfilePostViewComponent} from './profile-post-view/profile-post-view.component';
import {UsersSearchResultsComponent} from './users-search-results/users-search-results.component';
import {NgxSpinnerModule} from 'ngx-spinner';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {AlertComponent} from './alert/alert.component';
import {NotificationsComponent} from './notifications/notifications.component';
import {NotificationBoxComponent} from './notification-box/notification-box.component';
import {ClickOutsideModule} from 'ng-click-outside';
import {MomentModule} from 'ngx-moment';


@NgModule({
  declarations: [
    PostViewUnauthorizedComponent,
    ScrollTopButtonComponent,
    ConnectionsTabComponent,
    UserSubscribersInfoBoxComponent,
    UserSubscriptionsInfoBoxComponent,
    PostViewComponent,
    CommentBoxComponent,
    ProfilePostViewComponent,
    UsersSearchResultsComponent,
    AlertComponent,
    NotificationsComponent,
    NotificationBoxComponent,
  ],
  exports: [
    PostViewUnauthorizedComponent,
    ScrollTopButtonComponent,
    ConnectionsTabComponent,
    UserSubscribersInfoBoxComponent,
    UserSubscriptionsInfoBoxComponent,
    PostViewComponent,
    CommentBoxComponent,
    ProfilePostViewComponent,
    UsersSearchResultsComponent,
    CommonModule,
    NgxSpinnerModule,
    FormsModule,
    AlertComponent,
    InfiniteScrollModule,
    NotificationsComponent,
    NotificationBoxComponent,
  ],
  imports: [
    ClickOutsideModule,
    CommonModule,
    FormsModule,
    NgbModule,
    RouterModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    InfiniteScrollModule,
    MomentModule.forRoot({
      relativeTimeThresholdOptions: {
        m: 59
      }
    })]
})
export class SharedModule {
}
