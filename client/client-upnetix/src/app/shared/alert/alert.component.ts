import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {

  @Output() hide = new EventEmitter<boolean>();

  constructor() {
  }

  ngOnInit(): void {
  }

  public goToTop(): void {
    this.hide.emit(true);
  }
}
