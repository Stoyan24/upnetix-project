import {Component, Input, OnInit} from '@angular/core';
import {SocketService} from '../../services/socket.service';
import {NotificationsService} from '../../services/notifications.service';
import {NotificationInterface} from '../../common/models/notification-models/notification.interface';
import {tap} from 'rxjs/operators';
import {animate, style, transition, trigger} from '@angular/animations';
import {ToastService} from '../../services/toast.service';
import {UserTokenInfo} from '../../common/models/UserTokenInfo';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss'],
  animations: [
    trigger('countAnimation', [
      transition(':increment', [
        style({opacity: 0, transform: 'translateY(-30%)'}),
        animate('300ms ease', style({opacity: 1, transform: 'translateY(-2%)'})),
      ]),
      transition(':decrement', [
        animate('300ms ease', style({transform: 'translateY(+20%)'})),
      ]),
      transition(':leave', [
        animate('1000ms', style({ transform: 'rotateY(180deg)', opacity: 0 }))
      ]),
    ]),
  ]
})
export class NotificationsComponent implements OnInit {

  @Input() userInfo: UserTokenInfo;
  public notifications: NotificationInterface[] = [];
  public isShown = true;
  public notificationsNumber = 0;
  public alert = false;
  public color = this.notificationsNumber > 0 ? 'red' : 'white';

  constructor(
    private readonly socketService: SocketService,
    private readonly notificationsService: NotificationsService,
    private readonly toasterService: ToastService,
  ) {
  }

  ngOnInit(): void {
    this.socketService.getNotification(this.userInfo.userName).subscribe((n: NotificationInterface) => {
      this.notifications.unshift(n);
      this.notificationsNumber++;
      this.alert = true;
    });
    this.notificationsService.getUserNotifications(this.userInfo.userId)
      .pipe(
        tap((n) => {
          this.notificationsNumber = n.reduce((acc, i) => i.read === false ? ++acc : acc, 0);
          if (this.notificationsNumber > 0) { this.alert = true; }
        })
      )
      .subscribe((n: NotificationInterface[]) => {
        this.notifications = n;
      });
    this.socketService.getGlobalMsg().subscribe((msg) => {
      this.toasterService.show(msg, {
        delay: 4000,
        autohide: true
      });
    });
  }

  public markAsRead(notificationId): void {
    this.notificationsService.readNotification(notificationId).subscribe((n: NotificationInterface) => {
      const index = this.notifications.findIndex((i: NotificationInterface) => i.notificationId === n.notificationId);
      this.notifications[index].read = n.read;
      if (this.notificationsNumber > 0) { this.notificationsNumber--; }
    });
  }

  public showNotifications(): void {
    this.isShown = !this.isShown;
    this.alert = false;
  }

  public notificationNullify(): void {
    if (!this.alert) {
      this.alert = false;
    }
    this.isShown = true;
  }

}
