import {Component, Input, OnInit} from '@angular/core';
import {AllPostsInterface} from '../../common/models/post-models/all-posts.interface';

@Component({
  selector: 'app-post-view-unauthorized',
  templateUrl: './post-view-unauthorized.component.html',
  styleUrls: ['./post-view-unauthorized.component.scss']
})
export class PostViewUnauthorizedComponent implements OnInit {

  @Input() post: AllPostsInterface;

  constructor() { }

  ngOnInit(): void {
  }

}
