import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostViewUnauthorizedComponent } from './post-view-unauthorized.component';

describe('PostViewUnauthorizedComponent', () => {
  let component: PostViewUnauthorizedComponent;
  let fixture: ComponentFixture<PostViewUnauthorizedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostViewUnauthorizedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostViewUnauthorizedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
