import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AllPostsInterface} from '../../common/models/post-models/all-posts.interface';
import {PostsService} from '../../services/posts.service';
import {OnDemandPreloadService} from '../../core/load-strategys/load-strategy.service';

@Component({
  selector: 'app-post-view',
  templateUrl: './post-view.component.html',
  styleUrls: ['./post-view.component.scss'],
})
export class PostViewComponent implements OnInit {

  @Input() post: AllPostsInterface;
  @Output() postId = new EventEmitter<string>();

  constructor(
    private readonly postsService: PostsService,
    private readonly preloadOnDemandService: OnDemandPreloadService
    ) {
  }

  ngOnInit(): void {
  }


  public like_unlike_Post(): void {
    this.postsService.likeUnlikePost(this.post.postId).subscribe((p: AllPostsInterface) => {
        this.post.likes = p.likes;
        this.post.isLiked = !this.post.isLiked;
      },
      (e) => console.log(e)
    );
  }

  public preloadBundle(routePath): void {
    this.preloadOnDemandService.startPreload(routePath);
  }

  public showCommentBox(): void {
    this.postId.emit(this.post.postId);
  }

}
