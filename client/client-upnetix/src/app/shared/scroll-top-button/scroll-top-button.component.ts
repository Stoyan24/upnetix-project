import {Component} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';


@Component({
  selector: 'app-scroll-top-button',
  templateUrl: './scroll-top-button.component.html',
  styleUrls: ['./scroll-top-button.component.scss'],
  animations: [
    trigger('openClose', [
      state('true', style({ opacity: 1, display: 'block'})),
      state('false', style({opacity: 0, display: 'none'})),
      transition('false <=> true', animate(100))
    ])
  ],
})
export class ScrollTopButtonComponent {

  public boolean;

  public showBool() {
    this.boolean = window.scrollY >= 700;
  }

  public scrollToTop(): void {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }
}
