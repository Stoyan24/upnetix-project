import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-connections-tab',
  templateUrl: './connections-tab.component.html',
  styleUrls: ['./connections-tab.component.scss'],
})
export class ConnectionsTabComponent implements OnInit {

  @Output() connectionType = new EventEmitter<string>();
  @Output() userId = new EventEmitter<string>();
  @Input() subscribers;
  @Input() subscriptions;

  constructor() { }

  ngOnInit(): void {
  }

  public sendType(type): void{
    this.connectionType.emit(type);
  }

  public sendUserId(userId: string): void {
    this.userId.emit(userId);
  }
}
