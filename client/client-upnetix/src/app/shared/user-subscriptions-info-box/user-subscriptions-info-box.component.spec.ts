import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserSubscriptionsInfoBoxComponent } from './user-subscriptions-info-box.component';

describe('UserSubscriptionsInfoBoxComponent', () => {
  let component: UserSubscriptionsInfoBoxComponent;
  let fixture: ComponentFixture<UserSubscriptionsInfoBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserSubscriptionsInfoBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserSubscriptionsInfoBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
