import {Component, EventEmitter, Input, Output} from '@angular/core';
import {UsersInterface} from '../../common/models/user-models/users.interface';

@Component({
  selector: 'app-user-subscriptions-info-box',
  templateUrl: './user-subscriptions-info-box.component.html',
  styleUrls: ['./user-subscriptions-info-box.component.scss']
})
export class UserSubscriptionsInfoBoxComponent {

  @Output() userId = new EventEmitter<string>();
  @Input() user: UsersInterface;
  @Input() buttonBool = true;

  constructor() {
  }

  public emitsUserId(): void {
    this.userId.emit(this.user.userId);
  }

}
