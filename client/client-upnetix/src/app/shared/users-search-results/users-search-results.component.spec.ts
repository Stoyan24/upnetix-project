import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersSearchResultsComponent } from './users-search-results.component';

describe('UsersSearchResultsComponent', () => {
  let component: UsersSearchResultsComponent;
  let fixture: ComponentFixture<UsersSearchResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersSearchResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersSearchResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
