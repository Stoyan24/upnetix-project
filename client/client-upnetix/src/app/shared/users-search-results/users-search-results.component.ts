import {Component, Input, OnInit} from '@angular/core';
import {UsersInterface} from '../../common/models/user-models/users.interface';

@Component({
  selector: 'app-users-search-results',
  templateUrl: './users-search-results.component.html',
  styleUrls: ['./users-search-results.component.scss']
})
export class UsersSearchResultsComponent implements OnInit {

  @Input() user: UsersInterface;

  constructor() { }

  ngOnInit(): void {
  }

}
