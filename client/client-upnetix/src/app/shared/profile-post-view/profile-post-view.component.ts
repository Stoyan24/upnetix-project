import {Component, Input, OnInit} from '@angular/core';
import {AllPostsInterface} from '../../common/models/post-models/all-posts.interface';
import {PostsService} from '../../services/posts.service';
import {UserAuthorInterface} from '../../common/models/post-models/user-author.interface';
import {OnDemandPreloadService} from '../../core/load-strategys/load-strategy.service';

@Component({
  selector: 'app-profile-post-view',
  templateUrl: './profile-post-view.component.html',
  styleUrls: ['./profile-post-view.component.scss']
})
export class ProfilePostViewComponent implements OnInit {

  @Input() post: AllPostsInterface;
  @Input() user: UserAuthorInterface;

  constructor(
    private readonly postsService: PostsService,
    private readonly preloadOnDemandService: OnDemandPreloadService
  ) { }

  ngOnInit(): void {
  }

  public preloadBundle(routePath): void {
    this.preloadOnDemandService.startPreload(routePath);
  }

  public like_unlike_Post(): void {
    this.postsService.likeUnlikePost(this.post.postId).subscribe((p: AllPostsInterface) => {
        this.post.likes = p.likes;
        this.post.isLiked = !this.post.isLiked;
      },
      (e) => console.log(e)
    );
  }

}
