import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CONFIG} from '../config/config';

@Injectable({
  providedIn: 'root'
})
export class HelperService {

  constructor(private readonly http: HttpClient) {}

  public checkIfUserNameExist(value: string): Observable<boolean>{
    return this.http.get<boolean>(`${CONFIG.API_DOMAIN_NAME}/users/u/${value}`);
  }

  public checkIfEmailNameExist(value: string): Observable<boolean>{
    return this.http.get<boolean>(`${CONFIG.API_DOMAIN_NAME}/users/e/${value}`);
  }

  public checkUserNameOREmailExist(value: string): Observable<boolean>{
    return this.http.get<boolean>(`${CONFIG.API_DOMAIN_NAME}/users/ue/${value}`);
  }
}
