import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  public getItem(key: string): string {
    return localStorage.getItem(key);
  }

  public setItem(key: string, value: string): void {
    localStorage.setItem(key, value);
  }

  public removeItem(key: string): void {
    localStorage.removeItem(key);
  }

  public clear(): void {
    localStorage.setItem('token', null);
  }

  public rememberMeSave(userName: string, imgUrl: string): void {
    localStorage.setItem('username', userName);
    localStorage.setItem('imgUrl', imgUrl);
  }

  public rememberMeGetNameItem(): string {
    return localStorage.getItem('username');
  }

  public rememberMeGetImgUrlItem(): string {
    return localStorage.getItem('imgUrl');
  }

  public clearLocalStorage(): void {
    localStorage.clear();
  }
}
