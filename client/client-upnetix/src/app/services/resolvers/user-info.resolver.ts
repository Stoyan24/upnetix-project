import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {UserProfileInterface} from '../../common/models/user-models/user-profile.interface';
import {UsersService} from '../users.service';

@Injectable({providedIn: 'root'})
export class UserInfoResolver implements Resolve<UserProfileInterface> {
  constructor(
    private readonly usersService: UsersService,
  ) {
  }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<UserProfileInterface> {
    const userId = route.paramMap.get('userId');
    return this.usersService.getUserByUserId(userId).pipe(
      map(user => {
        if (user) {
          return user;
        } else {
          return null;
        }
      })
    );
  }
}
