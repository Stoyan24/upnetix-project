import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {AllPostsInterface} from '../../common/models/post-models/all-posts.interface';
import {PostsService} from '../posts.service';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class AllPubicPostsResolver implements Resolve<AllPostsInterface[]> {
  constructor(private postsService: PostsService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<AllPostsInterface[]> {
    return this.postsService.getAllPublicPosts(0, 9).pipe(
      map(posts => {
        if (posts) {
          return posts;
        } else {
          return [];
        }
      })
    );
  }
}
