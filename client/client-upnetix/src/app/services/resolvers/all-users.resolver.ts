import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {UsersService} from '../users.service';
import {UsersInterface} from '../../common/models/user-models/users.interface';

@Injectable({providedIn: 'root'})
export class AllUsersResolver implements Resolve<UsersInterface[]> {
  constructor(
    private readonly usersService: UsersService,
  ) {
  }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<UsersInterface[]> {
    return this.usersService.getAllUsers().pipe(
      map(users => {
        if (users) {
          return users;
        } else {
          return null;
        }
      })
    );
  }
}
