import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {PostsService} from '../posts.service';
import {Observable} from 'rxjs';
import {delay, map} from 'rxjs/operators';
import {PostDetailsInterface} from '../../common/models/post-models/post-details.interface';

@Injectable({ providedIn: 'root' })
export class PostDetailsResolver implements Resolve<PostDetailsInterface> {
  constructor(private postsService: PostsService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<PostDetailsInterface> {
    const postId = route.paramMap.get('postId');
    return this.postsService.getPostById(postId).pipe(
      delay(200),
      map(posts => {
        if (posts) {
          return posts;
        } else {
          return null;
        }
      })
    );
  }
}
