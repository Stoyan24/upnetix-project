import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CONFIG} from '../config/config';
import {NotificationInterface} from '../common/models/notification-models/notification.interface';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {

  constructor(
    private readonly http: HttpClient,
  ) { }

  public getUserNotifications(userId: string): Observable<NotificationInterface[]> {
    return this.http.get<NotificationInterface[]>(`${CONFIG.API_DOMAIN_NAME}/notifications/${userId}/notifications`);
  }

  public readNotification(notificationId: string): Observable<NotificationInterface> {
    return this.http.put<NotificationInterface>(`${CONFIG.API_DOMAIN_NAME}/notifications/${notificationId}`, null);
  }
}
