import {Injectable} from '@angular/core';
import {Socket} from 'ngx-socket-io';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SocketService {

  constructor(
    private readonly socket: Socket,
  ) {
  }

  public getNewPost() {
    return this.socket
      .fromEvent('newPosts').pipe(
        map(data => data),
      );
  }

  public emitNewComment(data) {
    return this.socket.emit('newComment', data);
  }

  public getNewComments(namespace: string) {
    return this.socket.fromEvent(namespace);
  }

  public getNotification(namespace: string) {
    return this.socket.fromEvent(namespace);
  }

  public usersNumber(): Observable<number> {
    return this.socket.fromEvent('users');
  }

  public sendGlobalMsg(msg: string): void {
    this.socket.emit('globalMsg', msg);
  }

  public getGlobalMsg(): Observable<string> {
    return this.socket.fromEvent('mg');
  }
}
