import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {JwtHelperService} from '@auth0/angular-jwt';
import {UserTokenInfo} from '../common/models/UserTokenInfo';
import {StorageService} from './storage.service';
import {RegisterUserInterface} from '../common/auth-service.models/register-user.interface';
import {CONFIG} from '../config/config';
import {tap} from 'rxjs/operators';
import {SighInUserInterface} from '../common/auth-service.models/sighInUser.interface';
import {FbAuthService} from './fb-auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private loggedUserDataSubject$ = new BehaviorSubject<UserTokenInfo>(
    this.getUserDataIfAuthenticated()
  );

  public constructor(
    private readonly http: HttpClient,
    private readonly storage: StorageService,
    private readonly jwtService: JwtHelperService,
    private readonly afAuth: FbAuthService,
  ) { }

  public get loggedUserData$(): Observable<UserTokenInfo> {
    return this.loggedUserDataSubject$.asObservable();
  }

  public emitUserData(user: UserTokenInfo): void {
    this.loggedUserDataSubject$.next(user);
  }

  public getUserDataIfAuthenticated(): UserTokenInfo {
    const token: string = this.storage.getItem('token');

    if (token && this.jwtService.isTokenExpired(token)) {
      this.storage.removeItem('token');
      return null;
    }

    return token ? this.jwtService.decodeToken(token) : null;
  }

  public register(user: RegisterUserInterface): Observable<RegisterUserInterface> {
    this.afAuth.signUp(user.email, user.password, user.userAvatarUrl, user.userName);
    return this.http.post<RegisterUserInterface>(
      `${CONFIG.API_DOMAIN_NAME}/users`,
      user
    );
  }

  public signIn(user: SighInUserInterface): Observable<{ token: string }> {
    return this.http
      .post<{ token: string }>(`${CONFIG.API_DOMAIN_NAME}/session`, user)
      .pipe(
        tap(({ token }) => {
          this.storage.setItem('token', token);
          const decodedToken = this.getLoggedUserData();
          this.emitUserData(decodedToken);
          this.afAuth.signIn(
            decodedToken.email,
            user.password,
            );
        })
      );
  }

  public signOut(): Observable<any> {
    return this.http.delete(`${CONFIG.API_DOMAIN_NAME}/session`)
      .pipe(
        tap(
          () => {
            this.storage.removeItem('token');
            this.emitUserData(null);
            this.afAuth.signOut();
          })
      );
  }

  private getLoggedUserData(): UserTokenInfo {
    const authToken: string = this.storage.getItem('token');

    if (authToken && this.jwtService.isTokenExpired(authToken)) {
      this.storage.removeItem('token');
      return null;
    }

    return authToken ? this.jwtService.decodeToken(authToken) : null;
  }
}
