import {Injectable} from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class FbAuthService {

  constructor(
    private afAuth: AngularFireAuth,
  ) {}

  public signIn(usernameOrEmail: string, password: string): void {
    this.afAuth.signInWithEmailAndPassword(usernameOrEmail, password).then((e) => console.log('user in'));
  }

  public async signUp(email: string, password: string, photoUrl: string, userName: string): Promise<void> {
    await this.afAuth.createUserWithEmailAndPassword(email, password).then(
      (res) => {
        res.user.updateProfile({
          displayName: userName,
          photoURL: photoUrl,
        });
      }
    );
  }

  public signOut(): void {
    this.afAuth.signOut().then((e) => console.log('user out'));
  }
}
