import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AllPostsInterface} from '../common/models/post-models/all-posts.interface';
import {CONFIG} from '../config/config';
import {CommentInterface} from '../common/models/comment-models/comment.interface';
import {PostDetailsInterface} from '../common/models/post-models/post-details.interface';
import {LatLngInterface} from '../common/models/location-models/latlng.interface';
import {map} from 'rxjs/operators';
import {CreatePostInterface} from '../common/models/post-models/create-post.interface';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  public constructor(
    private readonly http: HttpClient,
  ) {
  }

  public getAllPublicPosts(skip, take): Observable<AllPostsInterface[]> {
    return this.http.get<AllPostsInterface[]>(`${CONFIG.API_DOMAIN_NAME}/posts`,
      {params: new HttpParams().set('take', take).set('skip', skip)},
    );
  }

  public getPublicPosts(skip, take): Observable<AllPostsInterface[]> {
    return this.http.get<AllPostsInterface[]>(`${CONFIG.API_DOMAIN_NAME}/posts/all`,
      {params: new HttpParams().set('take', take).set('skip', skip)},
    );
  }

  public likeUnlikePost(postId: string): Observable<AllPostsInterface> {
    return this.http.patch<AllPostsInterface>(`${CONFIG.API_DOMAIN_NAME}/posts/${postId}/like`, null);
  }

  public getPostComments(postId: string): Observable<CommentInterface[]> {
    return this.http.get<CommentInterface[]>(`${CONFIG.API_DOMAIN_NAME}/posts/${postId}/comments`);
  }

  public getPostById(postId: string): Observable<PostDetailsInterface> {
    return this.http.get<PostDetailsInterface>(`${CONFIG.API_DOMAIN_NAME}/posts/${postId}`);
  }

  public createPost(body: CreatePostInterface): Observable<PostDetailsInterface> {
    return this.http.post<PostDetailsInterface>(`${CONFIG.API_DOMAIN_NAME}/posts`, body);
  }


  public reverseGeocode(body: LatLngInterface): Observable<any> {
    return this.http.post<any>(`${CONFIG.API_DOMAIN_NAME}/posts/reversegeocode`, body).pipe(
      map(data => {
        console.log(data.results[0]);
        const address: {long: string, short: string} = {long: '', short: ''};
        address.short = data.results[0].address_components.reduce((acc, index) => {
          if (index.types.includes('administrative_area_level_1')){
            acc = acc + ' ' + index.short_name;
          }
          if (index.types.includes('country')){
            acc = acc + ' ' + index.long_name;
          }
          return acc;
        }, ' ');
        address.long = data.results[0].formatted_address;
        return address;
      })
    );
  }
}
