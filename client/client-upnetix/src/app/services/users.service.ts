import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {CONFIG} from '../config/config';
import {UsersInterface} from '../common/models/user-models/users.interface';
import {map} from 'rxjs/operators';
import {AllPostsInterface} from '../common/models/post-models/all-posts.interface';
import {UserProfileInterface} from '../common/models/user-models/user-profile.interface';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  public constructor(
    private readonly http: HttpClient,
  ) {
  }

  public getAllUsers(): Observable<UsersInterface[]> {
    return this.http.get<UsersInterface[]>(`${CONFIG.API_DOMAIN_NAME}/users`);
  }

  public getUserConnections(userId: string, type: string): Observable<UsersInterface[]> {
    return this.http.get<UsersInterface[]>(`${CONFIG.API_DOMAIN_NAME}/users/${userId}/connections/${type}`);
  }

  public connectionsAction(userId: string): Observable<UsersInterface[]> {
    return this.http.patch<any>(`${CONFIG.API_DOMAIN_NAME}/users/connections/${userId}`, null).pipe(
      map((u) => u.subscriptions)
    );
  }

  public getUserByUserId(userId: string): Observable<UserProfileInterface> {
    return this.http.get<UserProfileInterface>(`${CONFIG.API_DOMAIN_NAME}/users/profile/${userId}`);
  }

  public getUserByName(username: string): Observable<UsersInterface[]> {
    if (!username.trim()) {
      return of([]);
    }
    return this.http.get<UsersInterface[]>(`${CONFIG.API_DOMAIN_NAME}/users/${username}`);
  }

  public getUserPost(userId: string, type: string): Observable<AllPostsInterface[]> {
    return this.http.get<AllPostsInterface[]>(`${CONFIG.API_DOMAIN_NAME}/users/${userId}/posts/${type}`);
  }

  public getUserLikedPosts(userId: string): Observable<AllPostsInterface[]> {
    return this.http.get<AllPostsInterface[]>(`${CONFIG.API_DOMAIN_NAME}/users/${userId}/likes`);
  }

  public deleteUser(userId: string): Observable<{userId: string}> {
    return this.http.delete<{userId: string}>(`${CONFIG.API_DOMAIN_NAME}/users/${userId}`);
  }
}
