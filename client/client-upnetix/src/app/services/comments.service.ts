import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {NewCommentInterface} from '../common/models/comment-models/new-comment.interface';
import {Observable} from 'rxjs';
import {CommentInterface} from '../common/models/comment-models/comment.interface';
import {CONFIG} from '../config/config';

@Injectable({
  providedIn: 'root'
})
export class CommentsService {

  public constructor(
    private readonly http: HttpClient,
  ) {
  }

  public createComment(postId: string, body: NewCommentInterface): Observable<CommentInterface> {
    return this.http.post<CommentInterface>(`${CONFIG.API_DOMAIN_NAME}/comments/${postId}`, body);
  }

  public removeComment(commentId: string): Observable<{commentId: string}> {
    return this.http.delete<{commentId: string}>(`${CONFIG.API_DOMAIN_NAME}/comments/${commentId}`);
  }
}
