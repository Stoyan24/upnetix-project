import {Injectable} from '@angular/core';
import {AbstractControl, AsyncValidator, ValidationErrors} from '@angular/forms';
import {interval, Observable, of} from 'rxjs';
import {catchError, delayWhen, map} from 'rxjs/operators';
import {HelperService} from '../helper.service';

@Injectable({providedIn: 'root'})
export class UsernameAsyncValidator implements AsyncValidator {
  constructor(private helperService: HelperService) {
  }

  validate(
    ctrl: AbstractControl
  ): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> {
    return this.helperService.checkIfUserNameExist(ctrl.value).pipe(
      delayWhen(() => interval(1000)),
      map(isTaken => (isTaken ? {usernameAsyncValidator: true} : null)),
      catchError(() => of(null))
    );
  }
}
