import {Injectable} from '@angular/core';
import {AbstractControl, AsyncValidator, ValidationErrors} from '@angular/forms';
import {interval, Observable, of} from 'rxjs';
import {catchError, delayWhen, map} from 'rxjs/operators';
import {HelperService} from '../helper.service';

@Injectable({providedIn: 'root'})
export class LoginAsyncValidator implements AsyncValidator {
  constructor(private helperService: HelperService) {
  }

  validate(
    ctrl: AbstractControl
  ): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> {
    return this.helperService.checkUserNameOREmailExist(ctrl.value).pipe(
      delayWhen(() => interval(1000)),
      map(isTaken => (isTaken ? {loginAsyncValidator: true} : null)),
      catchError(() => of(null))
    );
  }
}
