import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-notfoundpage',
  templateUrl: './notfoundpage.component.html',
  styleUrls: ['./notfoundpage.component.scss']
})
export class NotfoundPageComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
