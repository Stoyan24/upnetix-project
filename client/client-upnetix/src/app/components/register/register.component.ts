import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {UsernameAsyncValidator} from '../../services/custom-async-validators/username.async-validator';
import {EmailAsyncValidator} from '../../services/custom-async-validators/email.async-validator';
import {JwtHelperService} from '@auth0/angular-jwt';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public registerForm: FormGroup;
  public imgResult;
  public userAvatarUrl = 'Choose file';

  constructor(
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly fb: FormBuilder,
    private readonly http: HttpClient,
    private readonly usernameAsyncValidator: UsernameAsyncValidator,
    private readonly emailAsyncValidator: EmailAsyncValidator,
    private readonly jwtService: JwtHelperService,
  ) {
  }

  ngOnInit(): void {
    this.registerForm = this.fb.group({
      userName: ['',
        [Validators.required, Validators.minLength(3), Validators.maxLength(20)],
        this.usernameAsyncValidator.validate.bind(this.usernameAsyncValidator)
      ],
      email: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(50), Validators.email],
        this.emailAsyncValidator.validate.bind(this.emailAsyncValidator)
      ],
      password: ['', [Validators.required, Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/)]],
      userBio: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(200)]],
    });
  }

  public register(): void {
    this.authService.register({...this.registerForm.value, userAvatarUrl: 'https://i.imgur.com/BVZgOWT.jpg'})
      .subscribe(
        () => {
          this.authService.signIn(
            {usernameOrEmail: this.registerForm.value.email, password: this.registerForm.value.password}
          ).subscribe(({token}) => {
            const {userName} = this.jwtService.decodeToken(token);
            this.router.navigate([`/users/${userName}/news_feed`]);
          });
        }
      );
  }

  public onChange(event) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.imgResult = reader.result;
      this.imgResult = this.imgResult.split(',').pop();
    };
  }

  public uploadPicture() {
    this.http.post<{ data }>('https://api.imgur.com/3/image',
      {image: this.imgResult}).subscribe((e) => {
        this.userAvatarUrl = e.data.link;
      },
      (e) => {
        console.log(e);
      }
    );
  }
}
