import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {StorageService} from '../../services/storage.service';
import {UserTokenInfo} from '../../common/models/UserTokenInfo';
import {JwtHelperService} from '@auth0/angular-jwt';
import {LoginAsyncValidator} from '../../services/custom-async-validators/login.async-validator';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;
  public bool = false;
  public fistTimeImgURl = this.storageService.rememberMeGetImgUrlItem() || 'https://images.theabcdn.com/i/33095849.jpg';
  public popUpState = false;
  public wrongInfo = '';

  constructor(
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly fb: FormBuilder,
    private readonly storageService: StorageService,
    private readonly jwtService: JwtHelperService,
    private readonly loginAsyncValidator: LoginAsyncValidator,
  ) {
  }

  public ngOnInit(): void {
    this.loginForm = this.fb.group({
      // tslint:disable-next-line:max-line-length
      usernameOrEmail: [this.storageService.rememberMeGetNameItem() || '',
        [Validators.required, Validators.minLength(3), Validators.maxLength(50)],
        this.loginAsyncValidator.validate.bind(this.loginAsyncValidator)],
      password: ['', [Validators.required, Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/)]]
    });
  }

  public login() {
    this.authService.signIn(this.loginForm.value).subscribe(({token}) => {
        const userData: UserTokenInfo = this.jwtService.decodeToken(token);
        if (this.bool) {
          this.storageService.rememberMeSave(userData.userName, userData.userAvatarUrl);
        }
        this.router.navigate([`/users/${userData.userName}/news_feed`]);
        this.popUpState = false;
      },
      (e) => {
        this.popUpState = true;
        this.wrongInfo = e.error.error;
      });
  }
}
