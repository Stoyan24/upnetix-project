import {Component, OnDestroy, OnInit} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {UploadPostComponent} from '../upload-post/upload-post.component';
import {UserTokenInfo} from '../../common/models/UserTokenInfo';
import {Observable, Subject, Subscription} from 'rxjs';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import {UsersInterface} from '../../common/models/user-models/users.interface';
import {debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';
import {UsersService} from '../../services/users.service';
import {OnDemandPreloadService} from '../../core/load-strategys/load-strategy.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit, OnDestroy {

  public value = '';
  private searchTerms = new Subject<string>();
  public userList$: Observable<UsersInterface[]>;
  public loggedUser: UserTokenInfo;
  private loggedUserSubscription: Subscription;

  constructor(
    private readonly authService: AuthService,
    private readonly router: Router,
    private modalService: NgbModal,
    private readonly usersService: UsersService,
    private readonly preloadOnDemandService: OnDemandPreloadService,
  ) {
  }

  public ngOnInit(): void {
    this.loggedUserSubscription = this.authService.loggedUserData$.subscribe(
      (data: UserTokenInfo) => this.loggedUser = data
    );
    this.userList$ = this.searchTerms.pipe(
      // wait 300ms after each keystroke before considering the term
      debounceTime(300),

      // ignore new term if same as previous term
      distinctUntilChanged(),

      // switch to new search observable each time the term changes
      switchMap((username: string) => this.usersService.getUserByName(username)),
    );
  }

  public ngOnDestroy(): void {
    this.loggedUserSubscription.unsubscribe();
  }

  search(term: string): void {
    this.searchTerms.next(term);
  }

  public signOut(): void {
    this.authService
      .signOut()
      .subscribe(
        () => {
          this.router.navigate(['home']);
        });
  }

  public open(): void {
    this.modalService.open(UploadPostComponent, {centered: true, size: 'lg', scrollable: true});
  }

  public nullify(): void {
    this.value = '';
    this.searchTerms.next(''.trim());
  }

  public preloadBundle(routePath): void {
    this.preloadOnDemandService.startPreload(routePath);
  }
}
