import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {PostsService} from '../../services/posts.service';
import {LatLngInterface} from '../../common/models/location-models/latlng.interface';
import {LocationInterface} from '../../common/models/location-models/location.inteface';
import {PostStatus} from '../../common/enums/post-status.enum';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-upload-post',
  templateUrl: './upload-post.component.html',
  styleUrls: ['./upload-post.component.scss']
})
export class UploadPostComponent implements OnInit {

  public uploadForm: FormGroup;
  public isCollapsed = true;
  public bool = false;
  public location: LocationInterface = {
    formatted_address_long: undefined,
    formatted_address_short: undefined,
    lat: undefined,
    lng: undefined,
  };

  public imgResult;
  public photoUrl = 'Choose file';

  public options: { zoom: number, center: LatLngInterface };

  public markerPosition: LatLngInterface;
  public markerOptions: google.maps.MarkerOptions;


  constructor(
    private readonly fb: FormBuilder,
    private readonly http: HttpClient,
    private readonly postsService: PostsService,
    private modalService: NgbModal,
  ) {
  }

  ngOnInit(): void {
    this.uploadForm = this.fb.group({
      title: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(40)]],
      description: ['', [Validators.required, Validators.minLength(20), Validators.maxLength(200)]]
    });
    navigator.geolocation.getCurrentPosition(position => {
      this.options = {
        center: {
          lat: position.coords.latitude,
          lng: position.coords.longitude,
        },
        zoom: 14,
      };
      this.markerPosition = {
        lat: position.coords.latitude,
        lng: position.coords.longitude,
      };
      this.markerOptions = {
        animation: google.maps.Animation.DROP,
      };
    });
  }

  public uploadPost(): void {
    this.postsService.createPost({
      status: this.bool ? PostStatus.PRIVATE : PostStatus.PUBLIC,
      title: this.uploadForm.value.title,
      description: this.uploadForm.value.description,
      photoUrl: this.photoUrl,
      location: this.location,
    }).subscribe(
      (post) => {
        this.modalService.dismissAll();
      },
      (d) => console.log(d));
  }

  public addMarker(event: google.maps.MouseEvent): void {
    this.markerPosition = event.latLng.toJSON();
    this.postsService.reverseGeocode(this.markerPosition).subscribe(
      (address) => {
        this.location.formatted_address_long = address.long;
        this.location.formatted_address_short = address.short;
        this.location.lat = this.markerPosition.lat;
        this.location.lng = this.markerPosition.lng;
      },
      (e) => console.log(e)
    );
  }

  public onChange(event): void {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.imgResult = reader.result;
      this.imgResult = this.imgResult.split(',').pop();
    };
  }

  public uploadPicture(): void {
    this.http.post<{ data }>('https://api.imgur.com/3/image',
      {image: this.imgResult}).subscribe((e) => {
        this.photoUrl = e.data.link;
      },
      (e) => {
        console.log(e);
      }
    );
  }
}
