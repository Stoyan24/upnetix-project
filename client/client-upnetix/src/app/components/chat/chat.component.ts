import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {AngularFireAuth} from '@angular/fire/auth';
import {AfUserInterface} from '../../common/auth-service.models/afUser.interface';
import {MessageInterface} from '../../common/models/messages-models/message.interface';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss'],
})
export class ChatComponent implements OnInit {

  public messages: Observable<MessageInterface[]>;
  public user: AfUserInterface;
  public input: string;
  @Input() isOpen: boolean;

  @Output() bool = new EventEmitter<boolean>();


  constructor(
    public afs: AngularFirestore,
    private afAuth: AngularFireAuth,
  ) {
  }

  ngOnInit() {
    this.afAuth.authState.subscribe((u) => {
        if (u) {
          this.user = u;
        }
      }
    );
    this.messages = this.afs.collection<MessageInterface>('messages', ref => ref.orderBy('date', 'desc')).valueChanges();
  }

  public toggleChat(bool: boolean): void {
    this.bool.emit(bool);
  }

  add() {
    this.afs.collection('messages').add({
      content: this.input,
      date: Date.now(),
      authorName: this.user.displayName,
      photoURL: this.user.photoURL,
    });
    this.input = '';
  }
}
