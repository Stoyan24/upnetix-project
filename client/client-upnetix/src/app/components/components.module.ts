import {NgModule} from '@angular/core';
import {NavbarComponent} from './navbar/navbar.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {HomeComponent} from './home/home.component';
import {SharedModule} from '../shared/shared.module';
import {RouterModule} from '@angular/router';
import { NotfoundPageComponent } from './notfoundpage/notfoundpage.component';
import { ServerErrorComponent } from './servererror/servererror.component';
import { UploadPostComponent } from './upload-post/upload-post.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {CoreModule} from '../core/core.module';
import {GoogleMapsModule} from '@angular/google-maps';
import { ChatComponent } from './chat/chat.component';


@NgModule({
  declarations: [
    NavbarComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    NotfoundPageComponent,
    ServerErrorComponent,
    UploadPostComponent,
    ChatComponent,
  ],
    imports: [
        SharedModule,
        CoreModule,
        RouterModule,
        NgbModule,
        GoogleMapsModule,
    ],
  exports: [
    NavbarComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    NotfoundPageComponent,
    ServerErrorComponent,
    UploadPostComponent,
    ChatComponent,
  ],
  entryComponents: [UploadPostComponent]
})
export class ComponentsModule {
}
