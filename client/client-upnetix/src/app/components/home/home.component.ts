import {Component, OnInit} from '@angular/core';
import {AllPostsInterface} from '../../common/models/post-models/all-posts.interface';
import {ActivatedRoute} from '@angular/router';
import {catchError, delay, finalize, map, switchMap, tap} from 'rxjs/operators';
import {PostsService} from '../../services/posts.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {SocketService} from '../../services/socket.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public skip = 9;
  public posts: AllPostsInterface[];
  public newPosts: AllPostsInterface[] = [];
  public alertNotify = true;
  public disabler = false;


  constructor(
    private route: ActivatedRoute,
    private readonly postsService: PostsService,
    private spinner: NgxSpinnerService,
    private socketService: SocketService,
  ) {}

  ngOnInit(): void {
    this.route.data.pipe(map(posts => posts.posts)).subscribe((p) => this.posts = p);
    this.socketService.getNewPost().subscribe((p: AllPostsInterface) => {
      this.alertNotify = false;
      setTimeout(() => this.alertNotify = true, 3000);
      this.newPosts.unshift(p);
    });
  }

  public goToTop(bool): void {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
    this.alertNotify = bool;
  }

  public onScroll(): void {
    this.postsService.getPublicPosts(this.skip, 9)
      .pipe(
        tap(() => {
          this.spinner.show('load-more-spinner-home');
          this.disabler = true;
        }),
        delay(1000),
        tap((p) => this.posts.push(...p)),
        finalize(() => this.spinner.hide('load-more-spinner-home')),
        catchError(e => e)
      )
      .subscribe(
        () => {
          this.skip += 9;
          this.disabler = false;
        }
      );
  }
}
