import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomeComponent} from './components/home/home.component';
import {RegisterComponent} from './components/register/register.component';
import {LoginComponent} from './components/login/login.component';
import {ServerErrorComponent} from './components/servererror/servererror.component';
import {NotfoundPageComponent} from './components/notfoundpage/notfoundpage.component';
import {LoginGuard} from './core/guards/login.guard';
import {PublicPostResolver} from './services/resolvers/public-post.resolver';
import {OnDemandPreloadStrategy} from './core/load-strategys/OnDemandPreloadStrategy';
import {AdminGuard} from './core/guards/admin.guard';
import {AuthGuard} from './core/guards/auth.guard';


const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent, resolve: {posts: PublicPostResolver}, data: {state: 'home'}},
  {path: 'login', component: LoginComponent, canActivate: [LoginGuard], data: {state: 'login'}},
  {path: 'register', component: RegisterComponent, canActivate: [LoginGuard], data: {state: 'register'}},
  {path: 'users', loadChildren: () => import('./users/users.module').then(m => m.UsersModule)},
  {path: 'posts', loadChildren: () => import('./posts/posts.module').then(m => m.PostsModule), data: {preload: true}},
  {
    path: 'admin-panel', loadChildren: () => import('./admin-panel/admin-panel.module').then(m => m.AdminPanelModule),
    data: {preload: true},
    canActivate: [AuthGuard, AdminGuard]
  },
  {path: 'not-found', component: NotfoundPageComponent},
  {path: 'server-error', component: ServerErrorComponent},
  {path: '**', redirectTo: '/not-found'},

];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    preloadingStrategy: OnDemandPreloadStrategy
  })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
