import { Injectable } from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private readonly authService: AuthService,
    private readonly router: Router,
  ) {}

  public canActivate(): boolean {
    if (!this.authService.getUserDataIfAuthenticated()) {
      this.authService.signOut().subscribe(() => this.router.navigate(['login']), (e) => console.log(e));
      this.router.navigate(['login']);
      return false;
    }

    return true;
  }

}
