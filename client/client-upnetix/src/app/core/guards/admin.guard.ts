import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {UserRoleType} from '../../common/models/UserRoleType';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {
  constructor(
    private readonly authService: AuthService,
    private readonly router: Router,
  ) {}

  public canActivate(): boolean {
    const token = this.authService.getUserDataIfAuthenticated();
    if (!token.roles.includes(UserRoleType.Admin)) {
      this.router.navigate(['home']);
      return false;
    }
    return token.roles.includes(UserRoleType.Admin);
  }
}
