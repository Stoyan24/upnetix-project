import {Injectable} from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import {Observable} from 'rxjs';
import {StorageService} from '../../services/storage.service';
import {imgurKeys} from '../../common/secret-img-keys/imgur-key';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  public constructor(private readonly storage: StorageService) {
  }

  public intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const token = this.storage.getItem('token');
    let modifiedReq;
    if (request.url === 'https://api.imgur.com/3/image') {
      modifiedReq = request.clone({
        setHeaders: {
          Authorization: `Client-ID ${imgurKeys.Client_ID}`,
          Accept: 'application/json',
        }
      });
    } else {
      modifiedReq = request.clone({
        setHeaders: {
          Authorization: `Bearer ${token}`
        }
      });
    }
    return next.handle(modifiedReq);
  }
}
