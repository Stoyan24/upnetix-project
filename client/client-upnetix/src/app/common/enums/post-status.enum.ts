export enum PostStatus {
  PRIVATE = 'private',
  PUBLIC = 'public'
}
