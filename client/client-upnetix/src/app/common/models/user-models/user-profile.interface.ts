import {UsersInterface} from './users.interface';

export interface UserProfileInterface extends UsersInterface{
  subscribers: UsersInterface[];
  subscriptions: UsersInterface[];
}
