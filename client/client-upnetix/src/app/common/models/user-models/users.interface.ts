import {UserRoleType} from '../UserRoleType';

export interface UsersInterface {
  userName: string;
  userId: string;
  email: string;
  userBio: string;
  roles: UserRoleType[] | string[];
  userAvatarUrl: string;
}
