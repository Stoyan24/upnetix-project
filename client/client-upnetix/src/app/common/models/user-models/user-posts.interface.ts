import {UsersInterface} from './users.interface';
import {AllPostsInterface} from '../post-models/all-posts.interface';

export interface UserPostsInterface extends UsersInterface{
  posts: AllPostsInterface[];
}
