import {AllPostsInterface} from './all-posts.interface';

export interface PostDetailsInterface extends AllPostsInterface{
  description: string;
}
