import {LocationInterface} from '../location-models/location.inteface';
import {PostStatus} from '../../enums/post-status.enum';

export interface CreatePostInterface {
  status: PostStatus;
  title: string;
  description: string;
  photoUrl: string;
  location: LocationInterface;
}
