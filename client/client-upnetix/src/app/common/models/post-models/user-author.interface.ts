import {UserRoleType} from '../UserRoleType';

export interface UserAuthorInterface {
  userId: string;
  userName: string;
  email: string;
  userBio: string;
  userAvatarUrl: string;
  roles: UserRoleType[] | string[];
}
