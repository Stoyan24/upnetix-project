import {UserAuthorInterface} from './user-author.interface';
import {LocationInterface} from '../location-models/location.inteface';


export interface AllPostsInterface {
  postId: string;
  postedBy: UserAuthorInterface;
  status: string;
  isDeleted: boolean;
  title: string;
  photoUrl: string;
  createdOn: Date;
  location: LocationInterface;
  likes?: string;
  commentsCount?: number;
  isLiked: boolean;
}
