export interface MessageInterface {
  content: string;
  date: Date;
  authorName: string;
  photoURL: string;
}
