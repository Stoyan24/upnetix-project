import {UsersInterface} from '../user-models/users.interface';

export interface CommentInterface {
  commentId: string;
  commentContent: string;
  commentedBy: UsersInterface;
  createdOn: string;
}
