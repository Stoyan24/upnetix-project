import {UserRoleType} from './UserRoleType';

export interface UserTokenInfo {
  userName: string;
  userId: string;
  email: string;
  userBio: string;
  roles: UserRoleType[] | string[];
  userAvatarUrl: string;
}
