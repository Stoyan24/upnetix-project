import {UsersInterface} from '../user-models/users.interface';
import {NotificationType} from '../../enums/notification-type.enum';

export interface NotificationInterface {
  notificationId: string;
  to: UsersInterface;
  message: string;
  notifiedBy: Partial<UsersInterface>;
  read: boolean;
  createdOn: Date;
  updatedOn: Date;
  type: NotificationType;
  optionalInfo: {id: string, imgUrl: string};
}
