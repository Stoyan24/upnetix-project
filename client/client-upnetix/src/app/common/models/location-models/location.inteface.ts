export interface LocationInterface {
  formatted_address_short: string | undefined;
  formatted_address_long: string | undefined;
  lat: number | undefined;
  lng: number | undefined;
}
