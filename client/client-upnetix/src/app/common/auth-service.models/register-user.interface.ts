export interface RegisterUserInterface {
  userName: string;
  email: string;
  password: string;
  userBio: string;
  userAvatarUrl: string;
}
