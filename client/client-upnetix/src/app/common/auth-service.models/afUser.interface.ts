export interface AfUserInterface {
  uid: string;
  email: string;
  photoURL: string;
  displayName: string;
}
