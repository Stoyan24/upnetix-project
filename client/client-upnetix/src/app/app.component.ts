import {Component, OnDestroy, OnInit} from '@angular/core';
import {NavigationEnd, NavigationStart, Router, RouterEvent} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';
import {trigger, animate, style, group, query, transition, state} from '@angular/animations';
import {UserTokenInfo} from './common/models/UserTokenInfo';
import {Subscription} from 'rxjs';
import {AuthService} from './services/auth.service';

const routerTransition = trigger('routerTransition', [
  transition('details <=> *', [
    /* order */
    /* 1 */ query(':enter, :leave', style({ position: 'fixed', width: '100%' })
      , { optional: true }),
    /* 2 */ group([  // block executes in parallel
      query(':enter', [
        style({ transform: 'translateY(100%)' }),
        animate('0.5s ease-in-out', style({ transform: 'translateY(0%)' }))
      ], { optional: true }),
      query(':leave', [
        style({ transform: 'translateY(0%)' }),
        animate('0.5s ease-in-out', style({ transform: 'translateY(-100%)' }))
      ], { optional: true }),
    ])
  ]),
  transition('* <=> *', [
    /* order */
    /* 1 */ query(':enter, :leave', style({ position: 'fixed', width: '100%' })
      , { optional: true }),
    /* 2 */ group([  // block executes in parallel
      query(':enter', [
        style({ transform: 'translateX(100%)' }),
        animate('0.5s ease-in-out', style({ transform: 'translateX(0%)' }))
      ], { optional: true }),
      query(':leave', [
        style({ transform: 'translateX(0%)' }),
        animate('0.5s ease-in-out', style({ transform: 'translateX(-100%)' }))
      ], { optional: true }),
    ])
  ]),
]);

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [routerTransition,
    trigger('openClose', [
      state('open', style({
        height: '*',
        opacity: 1,
      })),
      state('closed', style({
        transform: 'translateX(+100%)',
      })),
      transition('open => closed', [
        animate('0.5s')
      ]),
      transition('closed => open', [
        animate('0.5s')
      ]),
    ]),
  ]
})
export class AppComponent implements OnDestroy, OnInit {
  public loggedUser: UserTokenInfo;
  private loggedUserSubscription: Subscription;
  public isOpen = false;


  constructor(
    public router: Router,
    private spinner: NgxSpinnerService,
    private readonly authService: AuthService,
  ) {
    router.events.subscribe(
      (event: RouterEvent): void => {
        if (event instanceof NavigationStart && !(event.url === '/login' || event.url === '/register')) {
          this.spinner.show();
        } else if (event instanceof NavigationEnd) {
          setTimeout(() => this.spinner.hide(), 600);
        }
      }
    );
  }

  getState(outlet) {
    return outlet.activatedRouteData.state;
  }

  toggle(bool: boolean) {
    this.isOpen = bool;
  }


  public ngOnInit(): void {
    this.loggedUserSubscription = this.authService.loggedUserData$.subscribe(
      (data: UserTokenInfo) => this.loggedUser = data
    );
  }

  public ngOnDestroy(): void {
    this.loggedUserSubscription.unsubscribe();
  }

}
