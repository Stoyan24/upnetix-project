## 9GRAM

### Description

9Gram is mixture of 9gag, Instagram and Facebook, when logged in you can upload posts,(add place where you post it), comment and like them, you can subscribe to other users(when you subscribe to some one you can see their private and liked post, also his subscribers, subscriptions), non logged user can only see post img and post title.There is admin panel where you can send global message to all users, see number of online users, delete users, track site activity.
Also 9Gram has global chat where you chat with everyone.There is remember me button when you login.In the navbar navigation you have search bar where you search other user, also notification "bell", where you can see your notification(when someone liked ,commented you post or subscribed to you).I have used socket.io so everything is realtime.

###Fist step
   - Files you should add at ROOT LEVEL
        * .env 
        ``` 
          PORT=3000
          DB_TYPE=mysql
          DB_HOST=localhost
          DB_PORT=3306
          DB_USERNAME=root
          DB_PASSWORD={your password}
          DB_DATABASE_NAME={your databese name}
          JWT_SECRET={your jwt-secret}
          JWT_EXPIRE_TIME=3600
     ```
      * ormconfig.json
      ``` 
          {
              "type": "mysql",
              "host": "localhost",
              "port": 3306,
              "username": "root",
              "password": {your password},
              "database": {your databese name},
              "synchronize": true,
              "logging": false,
              "entities": [
                "src/database/entities/**/*.ts"
              ],
              "migrations": [
                "src/database/migrations/**/*.ts"
              ],
              "cli": {
                "entitiesDir": "src/database/entities",
                "migrationsDir": "src/database/migrations"
              }
          }
        ```  
    
   - Files you should add at server/src/database/
        * ormconfig.ts
             
                     import { ConnectionOptions } from 'typeorm';
                     
                     const config: ConnectionOptions = {
                         type: 'mysql',
                         host: 'localhost',
                         port: 3306,
                         username: 'root',
                         password: 'your password',
                         database: 'your databese name',
                         entities: [__dirname + '/**/*.entity{.ts,.js}'],
                     
                         synchronize: false,
                     
                         migrationsRun: true,
                     
                         migrations: [__dirname + '/**/migrations/**/*{.ts,.js}'],
                         cli: {
                             migrationsDir: 'src/database/migrations',
                         },
                     };
                     
                     export = config;
 
   - When setting you database you should choose 
        * Create scheme named (same name with your .env file)
        * Charset/Collation - utf8

## Server side

 * Installation

```bash
$ npm install
```

* Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

* Seed

```bash
# seed for user, roles
$ npm run seed
    
```

* Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```
I don't hove test, yet !

* Running compodoc documentation


```bash
$ npx compodoc -p tsconfig.json -s --port 4032
```
Then open localhost:4032 to see documentation

### General guide

 - You can open Swagger for API calls
    * http://localhost:3000/swagger/upnetix/users
    * http://localhost:3000/swagger/upnetix/posts
    * http://localhost:3000/swagger/upnetix/comments
    * http://localhost:3000/swagger/upnetix/session
    

### Client

 * 'ng serve' it and ENJOY
 
### Technologies
    
   * JavaScript, 
   * TypeScript, 
   * NestJS, 
   * Angular 9, 
   * TypeORM, 
   * mySQL, 
   * HTML, 
   * SCSS,
   * Firebase,
   * Socket.IO
   * RxJS 
   * Bootstrap
   
### Authors and Contributors
    
  ##### - Stoyan Smilyanov - https://gitlab.com/Stoyan24

      
### License

   - This project is licensed under MIT License
