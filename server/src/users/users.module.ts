import {Module} from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {CommentEntity} from '../database/entities/comment.entity';
import {PostEntity} from '../database/entities/post.entity';
import {RoleEntity} from '../database/entities/role.entity';

import {IsUserIdValidConstraint} from '../common/pipes/custom-class-validator/IsUserIdValid';
import {IsEmailTakenConstraint} from '../common/pipes/custom-class-validator/isEmailTaken';
import {IsUsernameTakenConstraint} from '../common/pipes/custom-class-validator/isUsernameTaken';
import {NotificationEntity} from '../database/entities/notification.entity';
import {UserEntity} from '../database/entities/user.entity';
import {SocketModule} from '../gateways/socket.module';
import {NotificationsService} from '../notifications/notifications.service';
import {UsersController} from './users.controller';
import {UsersService} from './users.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            RoleEntity,
            UserEntity,
            PostEntity,
            CommentEntity,
            NotificationEntity,
        ]),
        SocketModule,
    ],
    controllers: [UsersController],
    providers: [
        UsersService,
        IsUsernameTakenConstraint,
        IsEmailTakenConstraint,
        IsUserIdValidConstraint,
        NotificationsService,
    ],
    exports: [UsersService]
})
export class UsersModule {
}
