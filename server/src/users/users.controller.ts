import {
    Body,
    Controller,
    Delete,
    Get,
    HttpCode,
    HttpStatus,
    Param,
    Patch,
    Post,
    Put,
    UseGuards,
    UseInterceptors,
    ValidationPipe
} from '@nestjs/common';
import {AuthGuard} from '@nestjs/passport';
import {
    ApiBadRequestResponse,
    ApiBearerAuth,
    ApiCreatedResponse,
    ApiForbiddenResponse,
    ApiNotFoundResponse,
    ApiOkResponse,
    ApiTags
} from '@nestjs/swagger';
import {actionsToMethods} from '../common/enums/post-actions';
import {UserRoleType} from '../common/enums/user-role';
import {Roles} from '../common/middleware/decorators/roles-decorator';
import {USER} from '../common/middleware/decorators/user-decorator';
import {RoleGuard} from '../common/middleware/guards/admin.guard';
import {TransformInterceptor} from '../common/middleware/transformer/interceptors/transform.interceptor';
import {ConnectionTypeValidation} from '../common/pipes/custom-validators/connection-type.validation';
import {PostsStatusValidation} from '../common/pipes/custom-validators/posts-status.validation';
import {UserIdValidation} from '../common/pipes/custom-validators/userId.validation';
import {PostEntity} from '../database/entities/post.entity';
import {UserEntity} from '../database/entities/user.entity';
import {ShowPostsDto} from '../posts/models/show-posts.dto';
import {CreateUserDto} from './models/create-user.dto';
import {ShowUserPostsDto} from './models/show-user-posts.dto';
import {ShowUserDto} from './models/show-user.dto';
import {ShowUsersDto} from './models/show-users.dto';
import {UpdateUserDto} from './models/update-user.dto';
import {UsersService} from './users.service';

@ApiTags('upnetix/users')
@Controller('upnetix/users')
export class UsersController {

    constructor(private readonly usersService: UsersService) {}

    @Get()
    @ApiBearerAuth()
    @ApiOkResponse({status: 201, description: 'Returns all users', type: ShowUsersDto})
    @UseInterceptors(new TransformInterceptor(ShowUsersDto))
    @Roles(UserRoleType.Admin)
    @UseGuards(AuthGuard('jwt'), RoleGuard)
    @HttpCode(HttpStatus.OK)
    public async getAllUsers(): Promise<UserEntity[]> {
        return await this.usersService.getAllUsers();
    }

    @Get(':searchTerm')
    @ApiBearerAuth()
    @ApiOkResponse({status: 201, description: 'Returns users that match search query', type: ShowUsersDto})
    @UseInterceptors(new TransformInterceptor(ShowUsersDto))
    @Roles(UserRoleType.User)
    @UseGuards(AuthGuard('jwt'), RoleGuard)
    @HttpCode(HttpStatus.OK)
    public async getUsersByUsername(@Param('searchTerm') searchTerm: string): Promise<UserEntity[]> {
        return await this.usersService.getUsersByUsername(searchTerm);
    }

    @Get('profile/:userId')
    @ApiBearerAuth()
    @ApiOkResponse({status: 201, description: 'Returns user that match id', type: ShowUserDto})
    @ApiNotFoundResponse({description: 'User with such id not found'})
    @UseInterceptors(new TransformInterceptor(ShowUserDto))
    @Roles(UserRoleType.User)
    @UseGuards(AuthGuard('jwt'), RoleGuard)
    @HttpCode(HttpStatus.OK)
    public async getUserById(
        @Param(new ValidationPipe({whitelist: true, transform: true, errorHttpStatusCode: 404})) userId: UserIdValidation,
        @USER() user: UserEntity): Promise<UserEntity> {
        return await this.usersService.getUserById(userId, user);
    }

    @Get(':userId/posts/:status')
    @ApiBearerAuth()
    @ApiOkResponse({status: 201, description: 'Returns user posts depending on status type', type: ShowPostsDto})
    @ApiNotFoundResponse({description: 'User has no posts'})
    @ApiForbiddenResponse({description: 'You are forbidden to access user privet posts must follow user first'})
    @UseInterceptors(new TransformInterceptor(ShowPostsDto))
    @Roles(UserRoleType.User)
    @UseGuards(AuthGuard('jwt'), RoleGuard)
    @HttpCode(HttpStatus.OK)
    public async getUserPosts(
        @Param(new ValidationPipe({whitelist: true, transform: true, errorHttpStatusCode: 404})) userId: UserIdValidation,
        @Param(new ValidationPipe({whitelist: true, transform: true})) status: PostsStatusValidation,
        @USER() user: UserEntity): Promise<PostEntity[]> {
        return await this.usersService[actionsToMethods[status.status]](userId, user);
    }

    @Get(':userId/likes')
    @ApiBearerAuth()
    @ApiOkResponse({status: 201, description: 'Returns user liked posts', type: ShowPostsDto})
    @ApiForbiddenResponse({description: 'You are forbidden to access user liked posts must follow user first'})
    @UseInterceptors(new TransformInterceptor(ShowPostsDto))
    @Roles(UserRoleType.User)
    @UseGuards(AuthGuard('jwt'), RoleGuard)
    @HttpCode(HttpStatus.OK)
    public async getUserLikedPosts(
        @Param(new ValidationPipe({whitelist: true, transform: true, errorHttpStatusCode: 404})) userId: UserIdValidation,
        @USER() user: UserEntity): Promise<PostEntity[]> {
        return await this.usersService.getUserLikedPosts(userId, user);
    }

    @Get(':userId/connections/:type')
    @ApiBearerAuth()
    @ApiOkResponse({status: 201, description: 'Returns user connections based on connection type query', type: ShowUserDto})
    @ApiForbiddenResponse({description: 'You are forbidden to access user connections must follow user first'})
    @UseInterceptors(new TransformInterceptor(ShowUsersDto))
    @Roles(UserRoleType.User)
    @UseGuards(AuthGuard('jwt'), RoleGuard)
    @HttpCode(HttpStatus.OK)
    public async getConnections(
        @Param(new ValidationPipe({whitelist: true, transform: true, errorHttpStatusCode: 404})) userId: UserIdValidation,
        @Param(new ValidationPipe({whitelist: true, transform: true})) type: ConnectionTypeValidation,
        @USER() user: UserEntity): Promise<UserEntity[]> {
        return await this.usersService.getConnections(userId, type, user);
    }

    @Put(':userId')
    @ApiBearerAuth()
    @ApiOkResponse({status: 201, description: 'Returns user connections based on connection type query', type: ShowUserDto})
    @ApiForbiddenResponse({description: 'You are forbidden to update other users accounts, only admin can do that sh*t!'})
    @UseInterceptors(new TransformInterceptor(ShowUsersDto))
    @Roles(UserRoleType.User)
    @UseGuards(AuthGuard('jwt'), RoleGuard)
    @HttpCode(HttpStatus.OK)
    public async updateUser(
        @Param(new ValidationPipe({whitelist: true, transform: true, errorHttpStatusCode: 404})) userId: UserIdValidation,
        @Body(new ValidationPipe({whitelist: true, transform: true})) body: UpdateUserDto,
        @USER() user: UserEntity): Promise<UserEntity> {
        return await this.usersService.updateUser(userId, user, body);
    }

    @Post()
    @ApiCreatedResponse({description: 'Creates user', type: ShowUserDto})
    @ApiBadRequestResponse({description: 'User name or email already exists'})
    @UseInterceptors(new  TransformInterceptor(ShowUserDto))
    @HttpCode(HttpStatus.CREATED)
    public async createUser(@Body(new ValidationPipe({whitelist: true, transform: true})) body: CreateUserDto): Promise<UserEntity> {
        return await this.usersService.createUser(body);
    }

    @Delete(':userId')
    @ApiBearerAuth()
    @ApiOkResponse({status: 201, description: 'Deletes user'})
    @ApiForbiddenResponse({description: 'You are forbidden to delete other users accounts, only admin can do that sh*t!'})
    @Roles(UserRoleType.User)
    @UseGuards(AuthGuard('jwt'), RoleGuard)
    @HttpCode(HttpStatus.OK)
    public async deleteUser(
        @Param(new ValidationPipe({whitelist: true, transform: true, errorHttpStatusCode: 404})) userId: UserIdValidation,
        @USER() user: UserEntity): Promise<{userId: string}> {
        return await this.usersService.deleteUser(userId, user);
    }

    @Patch('/connections/:userId')
    @ApiBearerAuth()
    @ApiOkResponse({status: 201, description: 'Subscribe to user'})
    @ApiBadRequestResponse({description: 'User with this id not found'})
    @UseInterceptors(new  TransformInterceptor(ShowUserDto))
    @Roles(UserRoleType.User)
    @UseGuards(AuthGuard('jwt'), RoleGuard)
    @HttpCode(HttpStatus.OK)
    public async connectionActions(
        @Param(new ValidationPipe({whitelist: true, transform: true, errorHttpStatusCode: 404})) userId: UserIdValidation,
        @USER() user: UserEntity
    ): Promise<UserEntity> {
        return await this.usersService.connectionActions(userId, user)
    }

    // register validation helpers
    @Get('/u/:userName')
    @HttpCode(HttpStatus.OK)
    @ApiOkResponse({description: 'Username is valid!'})
    @ApiBadRequestResponse({description: 'Username already exist!'})
    public async checkUsernameExist(@Param () userName: string ): Promise<boolean> {
        return this.usersService.checkUserExist(userName);
    }

    @Get('/e/:email')
    @HttpCode(HttpStatus.OK)
    @ApiOkResponse({description: 'Email is valid!'})
    @ApiBadRequestResponse({description: 'Email already exist!'})
    public async checkEmailExist(@Param() email: string): Promise<boolean> {
        return this.usersService.checkEmailExist(email);
    }

    @Get('/ue/:usernameOrEmail')
    @HttpCode(HttpStatus.OK)
    @ApiOkResponse({description: 'userNameOrEmail is valid!'})
    @ApiBadRequestResponse({description: 'userNameOrEmail does not exist already exist!'})
    public async checkNameOrEmailExist(@Param() usernameOrEmail): Promise<boolean> {
        return this.usersService.checkIfUserOrEmailExist(usernameOrEmail);
    }
}
