import {ApiProperty} from '@nestjs/swagger';
import {IsEmail, IsNotEmpty, IsOptional, IsString, IsUrl, Matches, MaxLength, MinLength} from 'class-validator';
import {IsEmailTaken} from '../../common/pipes/custom-class-validator/isEmailTaken';
import {IsUsernameTaken} from '../../common/pipes/custom-class-validator/isUsernameTaken';

export class UpdateUserDto {

    @ApiProperty({
        type: String,
        minLength: 3,
        maxLength: 20
    })
    @IsOptional()
    @IsString()
    @MinLength(3, {
        message: 'Username is too short. Minimal length is $constraint1 characters, but actual is $value'
    })
    @MaxLength(20, {
        message: 'Username is too long. Maximal length is $constraint1 characters, but actual is $value'
    })
    @IsNotEmpty()
    @IsUsernameTaken({message: '$value is already taken'})
    public userName: string;

    @ApiProperty({
        type: String,
        minLength: 10,
        maxLength: 50
    })
    @IsOptional()
    @IsString()
    @MinLength(10, {
        message: 'Email is too short. Minimal length is $constraint1 characters, but actual is $value'
    })
    @MaxLength(50, {
        message: 'Email is too long. Maximal length is $constraint1 characters, but actual is $value'
    })
    @IsNotEmpty()
    @IsEmail({}, {message: 'Invalid email!'})
    @IsEmailTaken({message: '$value is already taken'})
    public email: string;

    @ApiProperty()
    @IsOptional()
    @IsString()
    @IsNotEmpty()
    @Matches(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/, {
        message:
            'The password must be minimum eight characters, one letter and one number',
    })
    public password: string;

    @ApiProperty({
        type: String,
        minLength: 10,
        maxLength: 20
    })
    @IsOptional()
    @IsString()
    @IsNotEmpty()
    @MinLength(10, {
        message: 'Bio is too short. Minimal length is $constraint1 characters, but actual is $value'
    })
    @MaxLength(200, {
        message: 'Bio is too long. Maximal length is $constraint1 characters, but actual is $value'
    })
    public userBio: string;

    @ApiProperty()
    @IsOptional()
    @IsNotEmpty()
    @IsString()
    @IsUrl({}, {message: 'Not real url'})
    public userAvatarUrl: string;
}
