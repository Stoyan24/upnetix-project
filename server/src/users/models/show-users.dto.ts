import {ApiProperty} from '@nestjs/swagger';
import {Expose} from 'class-transformer';
import {Publish} from '../../common/middleware/transformer/decorators/publish';
import {RoleEntity} from '../../database/entities/role.entity';
import {ShowRolesDto} from '../../roles/models/show-roles.dto';

export class ShowUsersDto {

    @ApiProperty()
    @Publish()
    @Expose()
    public userId: string;

    @ApiProperty()
    @Publish()
    @Expose()
    public userName: string;

    @ApiProperty()
    @Publish()
    @Expose()
    public email: string;

    @ApiProperty()
    @Publish()
    @Expose()
    public userBio: string;

    @ApiProperty()
    @Publish()
    @Expose()
    public userAvatarUrl: string;

    @ApiProperty({type: ShowRolesDto})
    @Publish(ShowRolesDto)
    public roles: RoleEntity[];
}
