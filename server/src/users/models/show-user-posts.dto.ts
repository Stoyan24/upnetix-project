import {ApiProperty} from '@nestjs/swagger';
import {Publish} from '../../common/middleware/transformer/decorators/publish';
import {PostEntity} from '../../database/entities/post.entity';
import {ShowPostNoUserDto} from '../../posts/models/show-post-no-user.dto';

export class ShowUserPostsDto {

    @ApiProperty()
    @Publish()
    public userId: string;

    @ApiProperty()
    @Publish()
    public userName;

    @ApiProperty()
    @Publish()
    public email: string;

    @ApiProperty()
    @Publish()
    public userAvatarUrl: string;

    @ApiProperty({type: ShowPostNoUserDto})
    @Publish(ShowPostNoUserDto)
    public posts: PostEntity[]
}
