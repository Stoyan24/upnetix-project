import {ApiProperty} from '@nestjs/swagger';
import {Publish} from '../../common/middleware/transformer/decorators/publish';
import {RoleEntity} from '../../database/entities/role.entity';
import {UserEntity} from '../../database/entities/user.entity';
import {ShowRolesDto} from '../../roles/models/show-roles.dto';
import {ShowUsersDto} from './show-users.dto';

export class ShowUserDto {

    @ApiProperty()
    @Publish()
    public userId: string;

    @ApiProperty()
    @Publish()
    public userName;

    @ApiProperty()
    @Publish()
    public email: string;

    @ApiProperty()
    @Publish()
    public userBio: string;

    @ApiProperty()
    @Publish()
    public userAvatarUrl: string;

    @ApiProperty()
    @Publish(ShowRolesDto)
    public roles: RoleEntity[];

    @ApiProperty({type: ShowUserDto})
    @Publish(ShowUsersDto)
    public subscriptions: UserEntity[];

    @ApiProperty({type: ShowUserDto})
    @Publish(ShowUsersDto)
    public subscribers: UserEntity[];

}
