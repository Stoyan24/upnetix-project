import {HttpStatus, Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import {Like, Repository} from 'typeorm';
import {LoginDto} from '../auth/models/login.dto';
import {NotificationType} from '../common/enums/notification-type';
import {PostStatus} from '../common/enums/post-status';
import {UserRoleType} from '../common/enums/user-role';
import {UpnetixSystemError} from '../common/middleware/exceptions/upnetix-system.error';
import {ConnectionTypeValidation} from '../common/pipes/custom-validators/connection-type.validation';
import {UserIdValidation} from '../common/pipes/custom-validators/userId.validation';
import {CommentEntity} from '../database/entities/comment.entity';
import {PostEntity} from '../database/entities/post.entity';
import {RoleEntity} from '../database/entities/role.entity';
import {UserEntity} from '../database/entities/user.entity';
import {SocketGateway} from '../gateways/socket.gateway';
import {HelperService} from '../helper/helper.service';
import {NotificationsService} from '../notifications/notifications.service';
import {CreateUserDto} from './models/create-user.dto';
import {UpdateUserDto} from './models/update-user.dto';

@Injectable()
export class UsersService {

    public constructor(
        @InjectRepository(UserEntity) private readonly userRepository: Repository<UserEntity>,
        @InjectRepository(RoleEntity) private readonly roleRepository: Repository<RoleEntity>,
        @InjectRepository(PostEntity) private readonly postRepository: Repository<PostEntity>,
        @InjectRepository(CommentEntity) private readonly commentRepository: Repository<CommentEntity>,
        private readonly helperService: HelperService,
        private readonly socketGateway: SocketGateway,
        private readonly notificationService: NotificationsService,
    ) {
    }

    public async createUser(user: CreateUserDto): Promise<UserEntity> {

        const userAssignedRoles: RoleEntity = await this.roleRepository.findOne({roleType: UserRoleType.User});

        const userEntity: UserEntity = this.userRepository.create();
        userEntity.roles = [userAssignedRoles];
        userEntity.subscribers = Promise.resolve([]);
        userEntity.subscriptions = Promise.resolve([]);
        userEntity.userName = user.userName;
        userEntity.email = user.email;
        userEntity.password = await bcrypt.hash(user.password, 10);
        userEntity.userBio = user.userBio;
        userEntity.userAvatarUrl = user.userAvatarUrl;
        userEntity.likedPosts = Promise.resolve([]);
        userEntity.posts = Promise.resolve([]);
        userEntity.userCommends = Promise.resolve([]);

        return await this.userRepository.save(userEntity);
    }

    public async getAllUsers(): Promise<UserEntity[]> {
        try {
            return await this.userRepository.find()
        } catch (err) {
            throw new UpnetixSystemError(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    public async getUsersByUsername(searchTerm: string): Promise<UserEntity[]> {
        let users: UserEntity[];
        try {
            users = await this.userRepository.find({userName: Like(`%${searchTerm}%`)})
        } catch (err) {
            throw new UpnetixSystemError(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
        }

        return users;
    }

    public async getUserById({userId}: UserIdValidation, user: UserEntity): Promise<UserEntity> {
        let foundUser: UserEntity;

        if (userId === user.userId || this.helperService.isFollower(user, userId) || this.helperService.isAdmin(user.roles)) {
            try {
                foundUser = await this.userRepository.findOne({
                    where: {userId},
                    relations: [
                        'subscribers',
                        'subscriptions',
                    ]
                })
            } catch (err) {
                throw new UpnetixSystemError(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
            }
        } else {
            try {
                foundUser = await this.userRepository.findOne({
                    where: {userId},
                })
            } catch (err) {
                throw new UpnetixSystemError(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
            }
            foundUser.subscribers = Promise.resolve([]);
            foundUser.subscriptions = Promise.resolve([]);
        }

        return foundUser;
    }

    public async getUserPrivatePosts({userId}: UserIdValidation, user: UserEntity): Promise<PostEntity[]> {
        let posts: PostEntity[];
        if (userId === user.userId || this.helperService.isFollower(user, userId) || this.helperService.isAdmin(user.roles)) {
            try {
                posts = await this.postRepository
                    .createQueryBuilder('post')
                    .leftJoinAndSelect('post.postedBy', 'postedBy')
                    .leftJoinAndSelect('postedBy.roles', 'roles')
                    .where('postedBy.userId = :userId', {userId})
                    .andWhere('post.isDeleted = :isDeleted', {isDeleted: false})
                    .andWhere('post.status = :status', {status: PostStatus.PRIVATE})
                    .getMany();
            } catch (err) {
                throw new UpnetixSystemError(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
            }
        } else {
            throw new UpnetixSystemError('You must follow user to be able to see his posts', HttpStatus.FORBIDDEN)
        }
        return posts;
    }

    public async getUserPublicPosts({userId}: UserIdValidation): Promise<PostEntity[]> {
        let posts: PostEntity[];
        try {
            posts = await this.postRepository
                .createQueryBuilder('post')
                .leftJoinAndSelect('post.postedBy', 'postedBy')
                .leftJoinAndSelect('postedBy.roles', 'roles')
                .where('postedBy.userId = :userId', {userId})
                .andWhere('post.isDeleted = :isDeleted', {isDeleted: false})
                .andWhere('post.status = :status', {status: PostStatus.PUBLIC})
                .getMany();
        } catch (err) {
            throw new UpnetixSystemError(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
        }
        return posts;
    }

    public async getUserLikedPosts({userId}: UserIdValidation, user: UserEntity): Promise<PostEntity[]> {

        let foundUser: UserEntity;
        if (userId === user.userId || this.helperService.isFollower(user, userId) || this.helperService.isAdmin(user.roles)) {
            try {
                foundUser = await this.userRepository.findOne(
                    {
                        where: {userId},
                        relations: ['likedPosts'],
                    })
            } catch (err) {
                throw new UpnetixSystemError(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
            }
        } else {
            throw new UpnetixSystemError('You must follow user to be able to see his liked posts', HttpStatus.BAD_REQUEST)
        }

        return (foundUser as any).__likedPosts__;
    }

    public async getConnections({userId}: UserIdValidation, {type}: ConnectionTypeValidation, user: UserEntity): Promise<UserEntity[]> {
        let foundUser: UserEntity;
        if (userId === user.userId || this.helperService.isFollower(user, userId) || this.helperService.isAdmin(user.roles)) {

            try {
                foundUser = await this.userRepository.findOne(
                    {
                        where: {userId},
                        relations: [type]
                    })
            } catch (err) {
                throw new UpnetixSystemError(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
            }
        } else {
            throw new UpnetixSystemError('You must follow user to be able to see his connections', HttpStatus.BAD_REQUEST)
        }

        return (foundUser as any)[`__${type}__`];
    }

    public async updateUser({userId}: UserIdValidation, user: UserEntity, body: Partial<UpdateUserDto>): Promise<UserEntity> {
        let foundUser: UserEntity;
        try {
            foundUser = await this.userRepository.findOne(userId)
        } catch (err) {
            throw new UpnetixSystemError(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
        }
        if (body.hasOwnProperty('password')) {
            body.password = await bcrypt.hash(body.password, 10)
        }
        if (user.userId === foundUser.userId || this.helperService.isAdmin(user.roles)) {
            const updateUser: UserEntity = {...foundUser, ...body};
            try {
                foundUser = await this.userRepository.save(updateUser)
            } catch (err) {
                throw new UpnetixSystemError(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
            }
        } else {
            throw new UpnetixSystemError('You cant update other users profiles,except if you are admin!', HttpStatus.BAD_REQUEST)
        }

        return foundUser;
    }

    public async deleteUser({userId}: UserIdValidation, user: UserEntity): Promise<{ userId: string }> {

        let foundUser: UserEntity;
        try {
            foundUser = await this.userRepository.findOne(userId)
        } catch (err) {
            throw new UpnetixSystemError(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
        }
        if (user.userId === foundUser.userId || this.helperService.isAdmin(user.roles)) {
            try {
                foundUser = await this.userRepository.findOne(
                    {
                        where: {userId},
                        relations: ['posts', 'userCommends']
                    });
                for (const post of await foundUser.posts) {
                    await this.postRepository.delete(post.postId)
                }
                for (const comment of await foundUser.userCommends) {
                    await this.commentRepository.delete(comment.commentId)
                }
                await this.userRepository.delete(userId);
            } catch (err) {
                throw new UpnetixSystemError(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
            }
        } else {
            throw new UpnetixSystemError('You cant delete other users profiles,except if you are admin!', HttpStatus.BAD_REQUEST)
        }
        return {userId: foundUser.userId};
    }

    public async connectionActions({userId}: UserIdValidation, user: UserEntity): Promise<UserEntity> {
        const reseter = {
            __subscribers__: null,
            __subscriptions__: null
        };
        let foundUser: UserEntity;
        try {
            foundUser = await this.userRepository.findOne({
                where: {userId},
                relations: [
                    'subscribers',
                    'subscriptions'
                ]
            })
        } catch (err) {
            throw new UpnetixSystemError(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
        }
        if (this.helperService.isFollower(user, foundUser.userId)) {
            (foundUser as any).__subscribers__ = (foundUser as any).__subscribers__.filter((u: UserEntity) => u.userId !== user.userId);
            (user as any).__subscriptions__ = (user as any).__subscriptions__.filter((u: UserEntity) => u.userId !== foundUser.userId);
        } else {
            const newNotification = await this.notificationService.createNotification(
                user,
                foundUser.userId,
                'subscribed to you.',
                NotificationType.CONNECTION,
            );
            await this.socketGateway.notify(foundUser.userName, newNotification);
            (foundUser as any).__subscribers__ = [...(foundUser as any).__subscribers__, {...user, ...reseter}];
            (user as any).__subscriptions__ = [...(user as any).__subscriptions__, {...foundUser, ...reseter}];
        }

        await this.userRepository.save([foundUser, user]);

        return user;
    }

    public async findUserByUsername(usernameOrEmail: LoginDto | string): Promise<UserEntity> {
        const foundUser: UserEntity = await this.userRepository.findOne({
            where: [{
                userName: usernameOrEmail,
            }, {
                email: usernameOrEmail,
            }],
            relations: [
                'subscribers',
                'subscriptions'
            ],
        });

        if (foundUser === undefined) {
            throw new UpnetixSystemError(`Wrong username or email!`, HttpStatus.BAD_REQUEST);
        }
        return foundUser;
    }

    public async validateUserPassword(user: LoginDto): Promise<boolean> {
        const foundUser: UserEntity = await this.userRepository.findOne({
            where: [{
                userName: user.usernameOrEmail,
            }, {
                email: user.usernameOrEmail,
            }],
        });

        return bcrypt.compare(user.password, foundUser.password);
    }

    // help method for registration and login validations
    public async checkUserExist(userName: string): Promise<boolean> {
        const foundUser: UserEntity = await this.userRepository.findOne(userName);
        return !!foundUser;
    }

    public async checkEmailExist(email: string): Promise<boolean> {
        const foundUser: UserEntity = await this.userRepository.findOne(email);
        return !!foundUser;
    }

    public async checkIfUserOrEmailExist(usernameOrEmail): Promise<boolean> {
        const foundUser: UserEntity = await this.userRepository.findOne({
            where: [{
                userName: usernameOrEmail.usernameOrEmail,
            }, {
                email: usernameOrEmail.usernameOrEmail,
            }]
        });
        return !foundUser;
    }
}
