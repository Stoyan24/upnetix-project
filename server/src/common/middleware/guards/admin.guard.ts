import {CanActivate, ExecutionContext, Injectable} from '@nestjs/common';
import {Reflector} from '@nestjs/core';
import {RoleEntity} from '../../../database/entities/role.entity';
import {UserEntity} from '../../../database/entities/user.entity';
import {UserRoleType} from '../../enums/user-role';

@Injectable()
export class RoleGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {}

  public canActivate(context: ExecutionContext): boolean {
    const roles: UserRoleType[] = this.reflector.get<UserRoleType[]>('roles', context.getHandler());
    if (!roles) {
      return true;
    }
    const request = context.switchToHttp().getRequest();
    const user: UserEntity = request.user;
    const hasRole = () => user.roles.some((role: RoleEntity) => roles.includes(UserRoleType[role.roleType]));
    if (user && user.roles && hasRole()) {
      return true;
    }
  }
}
