import { SetMetadata } from '@nestjs/common';
import {UserRoleType} from '../../enums/user-role';

// tslint:disable-next-line: variable-name
export const Roles = (...roles: UserRoleType[]) => SetMetadata('roles', roles);
