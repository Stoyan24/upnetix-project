import 'reflect-metadata';

// tslint:disable-next-line:variable-name
export const Publish = <T>(dto?: T) => {
  // tslint:disable-next-line:only-arrow-functions
  return function(target: any, propertyKey: string) {
    const exposed = Reflect.getMetadata('dto:transformer', target) || [];
    if (dto) {
      exposed.push({
        key: propertyKey,
        dto,
      });
    } else {
      exposed.push({
        key: propertyKey,
        dto: null,
      });
    }
    Reflect.defineMetadata('dto:transformer', exposed, target);
  };
};
