import { ArgumentsHost, Catch, ExceptionFilter } from '@nestjs/common';
import { Response } from 'express';
import {UpnetixSystemError} from '../exceptions/upnetix-system.error';

@Catch(UpnetixSystemError)
export class UpnetixErrorFilter implements ExceptionFilter {
  public catch(exception: UpnetixSystemError, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();

    response.status(exception.code).json({
      status: exception.code,
      error: exception.message,
    });
  }
}
