import {HttpStatus, Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {registerDecorator, ValidationArguments, ValidationOptions, ValidatorConstraint, ValidatorConstraintInterface} from 'class-validator';
import {Repository} from 'typeorm';
import {UserEntity} from '../../../database/entities/user.entity';

@ValidatorConstraint({ async: true})
@Injectable()
export class IsUserIdValidConstraint implements ValidatorConstraintInterface {

    constructor(@InjectRepository(UserEntity) private readonly userRepository: Repository<UserEntity>) {
    }

    async validate(userId: any, args: ValidationArguments) {
        const isValid = await this.userRepository.findOne(userId);
        return !!isValid;
    }
}

export function IsUserIdValid(validationOptions?: ValidationOptions) {
    // tslint:disable-next-line:ban-types only-arrow-functions
    return function (object: Object, propertyName: string) {
        registerDecorator({
            target: object.constructor,
            propertyName,
            options: validationOptions,
            constraints: [],
            validator: IsUserIdValidConstraint
        });
    };
}
