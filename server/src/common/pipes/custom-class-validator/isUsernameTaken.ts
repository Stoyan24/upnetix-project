import {Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {registerDecorator, ValidationArguments, ValidationOptions, ValidatorConstraint, ValidatorConstraintInterface} from 'class-validator';
import {Repository} from 'typeorm';
import {UserEntity} from '../../../database/entities/user.entity';

@ValidatorConstraint({ async: true })
@Injectable()
export class IsUsernameTakenConstraint implements ValidatorConstraintInterface {

    constructor(@InjectRepository(UserEntity) private readonly userRepository: Repository<UserEntity>) {
    }

    async validate(userName: any, args: ValidationArguments) {
        const isValid = await this.userRepository.findOne({where: {userName}});
        return !isValid;
    }

}

export function IsUsernameTaken(validationOptions?: ValidationOptions) {
    // tslint:disable-next-line:ban-types only-arrow-functions
    return function (object: Object, propertyName: string) {
        registerDecorator({
            target: object.constructor,
            propertyName,
            options: validationOptions,
            constraints: [],
            validator: IsUsernameTakenConstraint
        });
    };
}
