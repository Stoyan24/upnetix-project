import {Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {registerDecorator, ValidationArguments, ValidationOptions, ValidatorConstraint, ValidatorConstraintInterface} from 'class-validator';
import {Repository} from 'typeorm';
import {CommentEntity} from '../../../database/entities/comment.entity';

@ValidatorConstraint({ async: true })
@Injectable()
export class IsCommentIdValidConstraint implements ValidatorConstraintInterface {

    constructor(@InjectRepository(CommentEntity) private readonly commentRepository: Repository<CommentEntity>) {
    }

    async validate(commentId: any, args: ValidationArguments) {
        const isValid = await this.commentRepository.findOne(commentId);
        return !!isValid;
    }

}

export function IsCommentIdValid(validationOptions?: ValidationOptions) {
    // tslint:disable-next-line:ban-types only-arrow-functions
    return function (object: Object, propertyName: string) {
        registerDecorator({
            target: object.constructor,
            propertyName,
            options: validationOptions,
            constraints: [],
            validator: IsCommentIdValidConstraint
        });
    };
}
