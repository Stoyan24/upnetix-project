import {Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {registerDecorator, ValidationArguments, ValidationOptions, ValidatorConstraint, ValidatorConstraintInterface} from 'class-validator';
import {Repository} from 'typeorm';
import {PostEntity} from '../../../database/entities/post.entity';

@ValidatorConstraint({ async: true })
@Injectable()
export class IsPostIdValidConstraint implements ValidatorConstraintInterface {

    constructor(@InjectRepository(PostEntity) private readonly postRepository: Repository<PostEntity>) {
    }

    async validate(postId: any, args: ValidationArguments) {
        const isValid = await this.postRepository.findOne(postId);
        return !!isValid;
    }

}

export function IsPostIdValid(validationOptions?: ValidationOptions) {
    // tslint:disable-next-line:ban-types only-arrow-functions
    return function (object: Object, propertyName: string) {
        registerDecorator({
            target: object.constructor,
            propertyName,
            options: validationOptions,
            constraints: [],
            validator: IsPostIdValidConstraint
        });
    };
}
