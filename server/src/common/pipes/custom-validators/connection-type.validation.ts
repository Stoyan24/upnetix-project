import {IsEnum} from 'class-validator';
import {ConnectionType} from '../../enums/connection-type';

export class ConnectionTypeValidation {

    @IsEnum(ConnectionType, {message: 'Type can only be subscribers or subscriptions -> $value is invalid!'})
    public type: ConnectionType;
}
