import {IsEnum} from 'class-validator';
import {PostStatus} from '../../enums/post-status';

export class PostsStatusValidation {

    @IsEnum(PostStatus, {message: 'Post can be only private and public -> $value is invalid!'})
    public status: PostStatus;
}
