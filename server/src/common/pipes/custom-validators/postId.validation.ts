import {IsPostIdValid} from '../custom-class-validator/IsPostIdValid';

export class PostIdValidation {

    @IsPostIdValid({message: 'Post with id $value not found!'})
    public postId: string
}
