import {IsUsernameTaken} from '../custom-class-validator/isUsernameTaken';

export class UsernameValidation {

    @IsUsernameTaken({message: 'User with username $value already exist!'})
    public userName: string;
}
