import {IsEmailTaken} from '../custom-class-validator/isEmailTaken';

export class EmailValidation {

    @IsEmailTaken({message: 'User with email $value already exist!'})
    public email: string;
}
