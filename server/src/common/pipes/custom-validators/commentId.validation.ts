import {IsCommentIdValid} from '../custom-class-validator/IsCommentIdValid';

export class CommentIdValidation {

    @IsCommentIdValid({message: 'Comment with id $value not found!'})
    public commentId: string
}
