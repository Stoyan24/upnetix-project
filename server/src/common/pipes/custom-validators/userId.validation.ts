import {IsUserIdValid} from '../custom-class-validator/IsUserIdValid';

export class UserIdValidation {

    @IsUserIdValid( {message: 'User with id $value not found!'})
    public userId: string
}
