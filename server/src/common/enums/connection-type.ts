export enum ConnectionType {
    SUBSCRIBERS = 'subscribers',
    SUBSCRIPTIONS = 'subscriptions',
}
