export enum UserRoleType {
    Banned = 'Banned',
    User = 'User',
    Admin = 'Admin',
}
