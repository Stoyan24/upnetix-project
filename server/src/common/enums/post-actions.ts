import {PostStatus} from './post-status';

export const actionsToMethods = {
    [PostStatus.PUBLIC]: 'getUserPublicPosts',
    [PostStatus.PRIVATE]: 'getUserPrivatePosts',
};
