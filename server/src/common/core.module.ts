import {Global, Module} from '@nestjs/common';
import { AuthModule } from '../auth/auth.module';
import { ConfigModule } from '../config/config.module';
import {HelperModule} from '../helper/helper.module';

@Global()
@Module({
  imports: [ConfigModule, AuthModule, HelperModule],
  exports: [ConfigModule, AuthModule, HelperModule],
})
export class CoreModule {}
