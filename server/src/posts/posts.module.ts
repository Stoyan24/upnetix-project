import {HttpModule, Module} from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {IsPostIdValidConstraint} from '../common/pipes/custom-class-validator/IsPostIdValid';
import {CommentEntity} from '../database/entities/comment.entity';
import {NotificationEntity} from '../database/entities/notification.entity';
import {PostEntity} from '../database/entities/post.entity';
import {RoleEntity} from '../database/entities/role.entity';
import {UserEntity} from '../database/entities/user.entity';
import {SocketModule} from '../gateways/socket.module';
import {NotificationsService} from '../notifications/notifications.service';
import {PostsController} from './posts.controller';
import {PostsService} from './posts.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            UserEntity,
            PostEntity,
            CommentEntity,
            RoleEntity,
            NotificationEntity,
        ]),
        HttpModule,
        SocketModule,
    ],
    controllers: [PostsController],
    providers: [PostsService, IsPostIdValidConstraint, NotificationsService],
    exports: [PostsService]
})
export class PostsModule {}
