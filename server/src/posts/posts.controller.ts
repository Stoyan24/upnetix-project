import {
    Body,
    Controller,
    Delete,
    Get,
    HttpCode,
    HttpStatus,
    Param,
    Patch,
    Post,
    Put, Query,
    UseGuards, UseInterceptors,
    ValidationPipe
} from '@nestjs/common';
import {AuthGuard} from '@nestjs/passport';
import {ApiBearerAuth, ApiCreatedResponse, ApiNotFoundResponse, ApiOkResponse, ApiTags} from '@nestjs/swagger';
import {ShowPostCommentsDto} from '../comments/models/show-post-comments.dto';
import {UserRoleType} from '../common/enums/user-role';
import {Roles} from '../common/middleware/decorators/roles-decorator';
import {USER} from '../common/middleware/decorators/user-decorator';
import {RoleGuard} from '../common/middleware/guards/admin.guard';
import {TransformInterceptor} from '../common/middleware/transformer/interceptors/transform.interceptor';
import {PostIdValidation} from '../common/pipes/custom-validators/postId.validation';
import {CommentEntity} from '../database/entities/comment.entity';
import {PostEntity} from '../database/entities/post.entity';
import {UserEntity} from '../database/entities/user.entity';
import {CreatePostDto} from './models/create-post.dto';
import {LatLngDto} from './models/latlng.dto';
import {ShowPostDto} from './models/show-post-dto';
import {ShowPostsDto} from './models/show-posts.dto';
import {UpdatePostDto} from './models/update-post.dto';
import {PostsService} from './posts.service';

@ApiTags('upnetix/posts')
@Controller('upnetix/posts')
export class PostsController {

    constructor(private readonly postsService: PostsService) {}

    @Get()
    @Roles(UserRoleType.User)
    @UseGuards(AuthGuard('jwt'), RoleGuard)
    @ApiOkResponse({status: 201, description: 'Returns all public posts', type: ShowPostsDto})
    @UseInterceptors(new TransformInterceptor(ShowPostsDto))
    @HttpCode(HttpStatus.OK)
    public async getAllPublicPostsLoggedUser(
        @USER() user: UserEntity,
        @Query('skip') skip: number,
        @Query('take') take: number,
    ): Promise<PostEntity[]> {
        return await this.postsService.getAllPublicPosts(user, skip, take);
    }

    @Get('all')
    @ApiOkResponse({status: 201, description: 'Returns all public posts', type: ShowPostsDto})
    @UseInterceptors(new TransformInterceptor(ShowPostsDto))
    @HttpCode(HttpStatus.OK)
    public async getAllPublicPosts(
        @Query('skip') skip: number,
        @Query('take') take: number,
    ): Promise<PostEntity[]> {
        return await this.postsService.getAllPublicPosts(undefined, skip, take);
    }

    @Get(':postId')
    @ApiBearerAuth()
    @ApiOkResponse({status: 201, description: 'Returns post that match id', type: ShowPostDto})
    @ApiNotFoundResponse({description: 'Post with such id not found'})
    @UseInterceptors(new TransformInterceptor(ShowPostDto))
    @Roles(UserRoleType.User)
    @UseGuards(AuthGuard('jwt'), RoleGuard)
    @HttpCode(HttpStatus.OK)
    public async getPostById(@Param(new ValidationPipe({
        whitelist: true,
        transform: true,
        errorHttpStatusCode: 404
    })) postId: PostIdValidation): Promise<PostEntity> {
        return await this.postsService.getPostById(postId);
    }

    @Get(':postId/comments')
    @ApiBearerAuth()
    @ApiOkResponse({status: 201, description: 'Returns post comments', type: ShowPostCommentsDto})
    @ApiNotFoundResponse({description: 'Post with such id not found'})
    @UseInterceptors(new TransformInterceptor(ShowPostCommentsDto))
    @Roles(UserRoleType.User)
    @UseGuards(AuthGuard('jwt'), RoleGuard)
    @HttpCode(HttpStatus.OK)
    public async getPostComments(@Param(new ValidationPipe({
        whitelist: true,
        transform: true,
        errorHttpStatusCode: 404
    })) postId: PostIdValidation): Promise<CommentEntity[]> {
        return await this.postsService.getPostComments(postId);
    }

    @Post()
    @ApiBearerAuth()
    @ApiCreatedResponse({description: 'Create post', type: ShowPostDto})
    @UseInterceptors(new TransformInterceptor(ShowPostDto))
    @Roles(UserRoleType.User)
    @UseGuards(AuthGuard('jwt'), RoleGuard)
    @HttpCode(HttpStatus.OK)
    public async createPost(
        @USER() user: UserEntity,
        @Body(new ValidationPipe({whitelist: true, transform: true})) body: CreatePostDto
    ): Promise<PostEntity> {
        return await this.postsService.createPost(user, body);
    }

    @Put(':postId')
    @ApiBearerAuth()
    @ApiOkResponse({status: 201, description: 'Updates post', type: ShowPostDto})
    @ApiNotFoundResponse({description: 'Post with such id not found'})
    @UseInterceptors(new TransformInterceptor(ShowPostDto))
    @Roles(UserRoleType.User)
    @UseGuards(AuthGuard('jwt'), RoleGuard)
    @HttpCode(HttpStatus.OK)
    public async updatePost(
        @Param(new ValidationPipe({
            whitelist: true,
            transform: true,
            errorHttpStatusCode: 404
        })) postId: PostIdValidation,
        @Body(new ValidationPipe({whitelist: true, transform: true})) body: UpdatePostDto,
        @USER() user: UserEntity,
    ): Promise<PostEntity> {
        return await this.postsService.updatePost(postId, body, user);
    }

    @Delete(':postId')
    @ApiBearerAuth()
    @ApiOkResponse({status: 201, description: 'Deletes posts', type: ShowPostDto})
    @ApiNotFoundResponse({description: 'Post with such id not found'})
    @UseInterceptors(new TransformInterceptor(ShowPostDto))
    @Roles(UserRoleType.Admin)
    @UseGuards(AuthGuard('jwt'), RoleGuard)
    @HttpCode(HttpStatus.OK)
    public async deletePost(@Param(new ValidationPipe({
        whitelist: true,
        transform: true,
        errorHttpStatusCode: 404
    })) postId: PostIdValidation): Promise<any> {
        return await this.postsService.deletePost(postId);
    }

    @Patch(':postId/like')
    @ApiBearerAuth()
    @ApiOkResponse({status: 201, description: 'Like posts', type: ShowPostDto})
    @ApiNotFoundResponse({description: 'Post with such id not found'})
    @UseInterceptors(new TransformInterceptor(ShowPostsDto))
    @Roles(UserRoleType.User)
    @UseGuards(AuthGuard('jwt'), RoleGuard)
    @HttpCode(HttpStatus.OK)
    public async likePost(
        @Param(new ValidationPipe({
            whitelist: true,
            transform: true,
            errorHttpStatusCode: 404
        })) postId: PostIdValidation,
        @USER() user: UserEntity
    ): Promise<PostEntity> {
        return await this.postsService.likePost(postId, user)
    }

    @Post('reversegeocode')
    @ApiBearerAuth()
    @ApiOkResponse({status: 201, description: 'Location information', type: Object})
    @ApiNotFoundResponse({description: 'Wrong location'})
    @Roles(UserRoleType.User)
    @UseGuards(AuthGuard('jwt'), RoleGuard)
    @HttpCode(HttpStatus.OK)
    public async reverseGeoCode(
        @Body(new ValidationPipe({whitelist: true, transform: true})) location: LatLngDto
    ): Promise<object> {
        return await this.postsService.reverseGeocode(location);
    }
}
