import {ApiProperty} from '@nestjs/swagger';
import {Expose, Type} from 'class-transformer';
import {PostStatus} from '../../common/enums/post-status';
import {Publish} from '../../common/middleware/transformer/decorators/publish';
import {UserEntity} from '../../database/entities/user.entity';
import {ShowUsersDto} from '../../users/models/show-users.dto';
import {LocationInterface} from './location.interface';

export class ShowPostsDto {

    @ApiProperty()
    @Publish()
    @Expose()
    public postId: string;

    @ApiProperty({type: ShowUsersDto})
    @Expose({name: '__postedBy__'})
    @Type((r) => ShowUsersDto)
    @Publish(ShowUsersDto)
    public postedBy: UserEntity;

    @ApiProperty()
    @Publish()
    @Expose()
    public status: PostStatus;

    @ApiProperty()
    @Publish()
    @Expose()
    public isDeleted: boolean;

    @ApiProperty()
    @Publish()
    @Expose()
    public title: string;

    @ApiProperty()
    @Publish()
    @Expose()
    public photoUrl: string;

    @ApiProperty()
    @Publish()
    @Expose()
    public createdOn: Date;

    @ApiProperty()
    @Publish()
    @Expose()
    public location: LocationInterface;

    @ApiProperty()
    @Publish()
    @Expose()
    public isLiked: boolean;

    @ApiProperty()
    @Publish()
    @Expose()
    public likes: string;

    @ApiProperty()
    @Publish()
    @Expose()
    public commentsCount: number;
}
