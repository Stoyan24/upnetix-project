import {ApiProperty} from '@nestjs/swagger';
import {IsNotEmpty} from 'class-validator';

export class LatLngDto {

    @ApiProperty()
    @IsNotEmpty()
    public lat: number | undefined;

    @ApiProperty()
    @IsNotEmpty()
    public lng: number | undefined;
}
