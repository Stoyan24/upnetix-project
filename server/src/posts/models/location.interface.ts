export class LocationInterface {
    // tslint:disable-next-line:variable-name
    formatted_address_short: string | undefined;
    // tslint:disable-next-line:variable-name
    formatted_address_long: string | undefined;
    lat: number | undefined;
    lng: number | undefined;
}
