import {ApiProperty} from '@nestjs/swagger';
import {IsEnum, IsNotEmpty, IsOptional, IsString, MaxLength, MinLength} from 'class-validator';
import {PostStatus} from '../../common/enums/post-status';

export class UpdatePostDto {

    @ApiProperty({enum: ['public', 'private']})
    @IsOptional()
    @IsNotEmpty()
    @IsEnum(PostStatus, {message: 'Post can only be public or private!'})
    public status: PostStatus;

    @ApiProperty({
        type: String,
        minLength: 4,
        maxLength: 40
    })
    @IsOptional()
    @IsString()
    @MinLength(4, {
        message: 'Title is too short. Minimal length is $constraint1 characters, but actual is $value'
    })
    @MaxLength(40, {
        message: 'Title is too long. Maximal length is $constraint1 characters, but actual is $value'
    })
    @IsNotEmpty()
    public title: string;

    @ApiProperty({
        type: String,
        minLength: 2,
        maxLength: 200
    })
    @IsOptional()
    @IsString()
    @MinLength(20, {
        message: 'Description is too short. Minimal length is $constraint1 characters, but actual is $value'
    })
    @MaxLength(200, {
        message: 'Description is too long. Maximal length is $constraint1 characters, but actual is $value'
    })
    @IsNotEmpty()
    public description: string;
}
