import {ApiProperty} from '@nestjs/swagger';
import {PostStatus} from '../../common/enums/post-status';
import {Publish} from '../../common/middleware/transformer/decorators/publish';
import {UserEntity} from '../../database/entities/user.entity';
import {ShowUsersDto} from '../../users/models/show-users.dto';
import {LocationInterface} from './location.interface';

export class ShowPostDto {

    @ApiProperty()
    @Publish()
    public postId: string;

    @ApiProperty()
    @Publish(ShowUsersDto)
    public postedBy: UserEntity;

    @ApiProperty()
    @Publish()
    public likes: number;

    @ApiProperty()
    @Publish()
    public commentsCount: number;

    @ApiProperty()
    @Publish()
    public status: PostStatus;

    @ApiProperty()
    @Publish()
    public isDeleted: boolean;

    @ApiProperty()
    @Publish()
    public description: string;

    @ApiProperty()
    @Publish()
    public title: string;

    @ApiProperty()
    @Publish()
    public photoUrl: string;

    @ApiProperty()
    @Publish()
    public createdOn: Date;

    @ApiProperty()
    @Publish()
    public isLiked: boolean;

    @ApiProperty()
    @Publish()
    public location: LocationInterface;
}
