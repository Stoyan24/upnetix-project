import {ApiProperty} from '@nestjs/swagger';
import {PostStatus} from '../../common/enums/post-status';
import {Publish} from '../../common/middleware/transformer/decorators/publish';
import {LocationInterface} from "./location.interface";

export class ShowPostNoUserDto {

    @ApiProperty()
    @Publish()
    public postId: string;

    @ApiProperty()
    @Publish()
    public status: PostStatus;

    @ApiProperty()
    @Publish()
    public isDeleted: boolean;

    @ApiProperty()
    @Publish()
    public title: string;

    @ApiProperty()
    @Publish()
    public photoUrl: string;

    @ApiProperty()
    @Publish()
    public createdOn: Date;

    @ApiProperty()
    @Publish()
    public isLiked: boolean;

    @ApiProperty()
    @Publish(LocationInterface)
    public location: LocationInterface;
}
