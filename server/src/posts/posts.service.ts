import {HttpService, HttpStatus, Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {map} from 'rxjs/operators';
import {Repository} from 'typeorm';
import {NotificationType} from '../common/enums/notification-type';
import {PostStatus} from '../common/enums/post-status';
import {UpnetixSystemError} from '../common/middleware/exceptions/upnetix-system.error';
import {PostIdValidation} from '../common/pipes/custom-validators/postId.validation';
import {CommentEntity} from '../database/entities/comment.entity';
import {PostEntity} from '../database/entities/post.entity';
import {RoleEntity} from '../database/entities/role.entity';
import {UserEntity} from '../database/entities/user.entity';
import {SocketGateway} from '../gateways/socket.gateway';
import {HelperService} from '../helper/helper.service';
import {MapperService} from '../helper/mapper.service';
import {NotificationsService} from '../notifications/notifications.service';
import {CreatePostDto} from './models/create-post.dto';
import {LatLngDto} from './models/latlng.dto';
import {UpdatePostDto} from './models/update-post.dto';

@Injectable()
export class PostsService {

    public constructor(
        @InjectRepository(UserEntity) private readonly userRepository: Repository<UserEntity>,
        @InjectRepository(RoleEntity) private readonly roleRepository: Repository<RoleEntity>,
        @InjectRepository(PostEntity) private readonly postRepository: Repository<PostEntity>,
        @InjectRepository(CommentEntity) private readonly commentRepository: Repository<CommentEntity>,
        private readonly helperService: HelperService,
        private readonly httpService: HttpService,
        private readonly socketGateway: SocketGateway,
        private readonly mapperService: MapperService,
        private readonly notificationService: NotificationsService,
    ) {
    }

    public async deletePost({postId}: PostIdValidation): Promise<PostEntity> {
        let post: PostEntity;
        try {
            post = await this.postRepository.findOne(postId);
        } catch (err) {
            throw new UpnetixSystemError(err.message, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        post.isDeleted = !post.isDeleted;
        return await this.postRepository.save(post);
    }

    public async getAllPublicPosts(user?: UserEntity | undefined, skip?: number, take?: number): Promise<PostEntity[]> {
        if (user) {
            const {likedPosts} = await this.userRepository.findOne({
                where: {userId: user.userId}, relations: ['likedPosts']
            });
            const postsId = (await likedPosts).map((e) => e.postId);
            const posts = await this.postRepository.find({
                where: {status: PostStatus.PUBLIC, isDeleted: false,},
                order: {createdOn: 'DESC'},
                skip,
                take,
            });
            for (const post of posts) {
                if (postsId.includes(post.postId)) {
                    post.isLiked = true;
                }
            }
            return posts;
        } else {
            try {
                return await this.postRepository.find({
                    where: {status: PostStatus.PUBLIC, isDeleted: false,},
                    order: {createdOn: 'DESC'},
                    skip,
                    take,
                });
            } catch (err) {
                throw new UpnetixSystemError(err.message, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    public async createPost(user: UserEntity, body: CreatePostDto): Promise<PostEntity> {
        let foundUser: UserEntity;
        try {
            foundUser = await this.userRepository.findOne({where: {userId: user.userId}, relations: ['posts']});
        } catch (err) {
            throw new UpnetixSystemError(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
        }
        const newPost: PostEntity = this.postRepository.create();
        newPost.status = body.status;
        newPost.title = body.title;
        newPost.description = body.description;
        newPost.photoUrl = body.photoUrl;
        newPost.location = body.location;
        delete (user as any).__subscribers__;
        delete (user as any).__subscriptions__;
        newPost.postedBy = Promise.resolve(user);
        const savedPost: PostEntity = await this.postRepository.save(newPost);
        (foundUser as any).__posts__.push(savedPost);
        await this.userRepository.save(foundUser);
        this.socketGateway.newPost(this.mapperService.toPostsDto(savedPost));
        return savedPost;
    }

    public async updatePost({postId}: PostIdValidation, body: Partial<UpdatePostDto>, user: UserEntity): Promise<PostEntity> {
        let post: PostEntity;
        try {
            post = await this.postRepository.findOne(postId);
        } catch (err) {
            throw new UpnetixSystemError(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
        }
        const checkOwner: boolean = user.userId === (post as any).__postedBy__.userId;
        let updatedPost: PostEntity;
        if (checkOwner) {
            updatedPost = {...post, ...body};
        } else if (this.helperService.isAdmin(user.roles)) {
            updatedPost = {...post, ...body};
        } else {
            throw new UpnetixSystemError('Only the owner of the post or admin can update it!', HttpStatus.BAD_REQUEST)
        }

        return await this.postRepository.save(updatedPost);
    }

    public async likePost({postId}: PostIdValidation, user: UserEntity): Promise<PostEntity> {
        let post: PostEntity;
        let foundUser: UserEntity;
        try {
            post = await this.postRepository.findOne(postId);
            foundUser = await this.userRepository.findOne({where: {userId: user.userId}, relations: ['likedPosts']})
        } catch (err) {
            throw new UpnetixSystemError(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
        }
        const checkLiked: boolean = (foundUser as any).__likedPosts__.some((p: PostEntity) => p.postId === post.postId);
        if (!checkLiked) {
            post.likes++;
            (foundUser as any).__likedPosts__.push(post);
            const newNotification = await this.notificationService.createNotification(
                user,
                (post as any).__postedBy__.userId,
                'liked your post',
                NotificationType.POST,
                {id: post.postId, imgUrl: post.photoUrl},
            );
            await this.socketGateway.notify((post as any).__postedBy__.userName, newNotification);
        } else {
            post.likes--;
            (foundUser as any).__likedPosts__ = (foundUser as any).__likedPosts__.filter((p: PostEntity) => p.postId !== post.postId)
        }
        await this.userRepository.save(foundUser);
        return await this.postRepository.save(post);
    }

    public async getPostById({postId}: PostIdValidation): Promise<PostEntity> {
        let post: PostEntity;
        try {
            post = await this.postRepository.findOne({where: {postId}});
        } catch (err) {
            throw new UpnetixSystemError(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
        }
        return post;
    }

    public async getPostComments({postId}: PostIdValidation): Promise<CommentEntity[]> {
        let postEntity: PostEntity;
        try {
            postEntity = await this.postRepository.findOne({where: {postId}, relations: ['comments']});
        } catch (err) {
            throw new UpnetixSystemError(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
        }
        const {comments} = postEntity;
        return comments;
    }

    public async reverseGeocode({lat, lng}: LatLngDto): Promise<object> {
        return this.httpService.get<object>(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&key=AIzaSyCLVgj3QcolTAMrF7FCGZzCNxiIEYeiA6k`)
            .pipe(
                map(response => response.data)
            );
    }
}
