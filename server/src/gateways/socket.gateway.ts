import {
    OnGatewayConnection,
    OnGatewayDisconnect, OnGatewayInit,
    SubscribeMessage,
    WebSocketGateway,
    WebSocketServer
} from '@nestjs/websockets';
import {Server, Socket} from 'socket.io';
import {MapperService} from '../helper/mapper.service';

@WebSocketGateway(8080)
export class SocketGateway implements OnGatewayConnection, OnGatewayDisconnect, OnGatewayInit {

    constructor(
        private readonly mapperService: MapperService,
    ) {
    }

    public users = 0;

    @WebSocketServer()
    server: Server;

    @SubscribeMessage('newPosts')
    newPost(data: any) {
        this.server.emit('newPosts', data)
    }

    @SubscribeMessage('newComment')
    newComment(client: Socket, data: any) {
        client.broadcast.emit(data.namespace, data.data)
    }

    @SubscribeMessage('globalMsg')
    sendToEveryone(client: Socket, data: any) {
        client.broadcast.emit('mg', data)
    }

    @SubscribeMessage('notify')
    notify(username: string, data: any, ): void {
        if (data === null){
            return
        }
        this.server.emit(username, this.mapperService.mapNotificationDto(data));
    }

    async afterInit() {
        console.log(`gateway initialized`);
    }

    async handleConnection(client: WebSocket) {
        this.users++;
        this.server.emit('users', this.users);
    }

    async handleDisconnect(client: WebSocket) {
        this.users--;
        this.server.emit('users', this.users);
    }
}
