import { Module } from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {NotificationEntity} from '../database/entities/notification.entity';
import {SocketGateway} from './socket.gateway';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            NotificationEntity,
        ]),
    ],
    providers: [SocketGateway],
    exports: [SocketGateway],
})
export class SocketModule {}
