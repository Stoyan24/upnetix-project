import {NestFactory} from '@nestjs/core';
import {DocumentBuilder, SwaggerModule} from '@nestjs/swagger';
import {useContainer} from 'class-validator';
import * as compression from 'compression';
import * as rateLimit from 'express-rate-limit';
import * as helmet from 'helmet';
import 'reflect-metadata';
import {Container} from 'typedi';
import {AppModule} from './app.module';
import {AuthModule} from './auth/auth.module';
import {CommentsModule} from './comments/comments.module';
import {UpnetixErrorFilter} from './common/middleware/filters/upnetix-error.filter';
import {ConfigService} from './config/config.service';
import {PostsModule} from './posts/posts.module';
import {UsersModule} from './users/users.module';

async function bootstrap() {
    const app = await NestFactory.create(AppModule);
    app.useGlobalFilters(new UpnetixErrorFilter());
    useContainer(Container);

    const optionsUsers = new DocumentBuilder().addBearerAuth()
        .setTitle('Upnetix example / Users')
        .setDescription('The Upnetix API description for users related commands')
        .setVersion('1.0')
        .addTag('users')
        .build();
    const userDocument = SwaggerModule.createDocument(app, optionsUsers, {include: [UsersModule]});
    SwaggerModule.setup('swagger/upnetix/users', app, userDocument);

    const optionsPosts = new DocumentBuilder().addBearerAuth()
        .setTitle('Upnetix example / Posts')
        .setDescription('The Upnetix API description for posts related commands')
        .setVersion('1.0')
        .addTag('posts')
        .build();
    const postDocument = SwaggerModule.createDocument(app, optionsPosts, {include: [PostsModule]});
    SwaggerModule.setup('swagger/upnetix/posts', app, postDocument);

    const optionsComments = new DocumentBuilder().addBearerAuth()
        .setTitle('Upnetix example / Comments')
        .setDescription('The Upnetix API description for comments related commands')
        .setVersion('1.0')
        .addTag('comments')
        .build();
    const commentDocument = SwaggerModule.createDocument(app, optionsComments, {include: [CommentsModule]});
    SwaggerModule.setup('swagger/upnetix/comments', app, commentDocument);

    const optionsSession = new DocumentBuilder().addBearerAuth()
        .setTitle('Upnetix example / Session')
        .setDescription('The Upnetix API description for login/logout related commands')
        .setVersion('1.0')
        .addTag('session')
        .build();
    const sessionDocument = SwaggerModule.createDocument(app, optionsSession, {include: [AuthModule]});
    SwaggerModule.setup('swagger/upnetix/session', app, sessionDocument);

// somewhere in your initialization file
    app.use(helmet());
    app.enableCors();

// somewhere in your initialization file
    app.use(
        rateLimit({
            windowMs: 30 * 60 * 1000, // 15 minutes
            max: 1000, // limit each IP to 100 requests per windowMs
        }),
    );
    app.use(compression());

    useContainer(app.select(AppModule), {fallbackOnErrors: true});
    await app.listen(app.get(ConfigService).port || 3000);

}

bootstrap();
