import {ApiProperty} from '@nestjs/swagger';
import {Publish} from '../../common/middleware/transformer/decorators/publish';
import {UserEntity} from '../../database/entities/user.entity';
import {ShowUsersDto} from '../../users/models/show-users.dto';

export class ShowPostCommentsDto {

    @ApiProperty()
    @Publish()
    public commentId: string;

    @ApiProperty()
    @Publish()
    public commentContent: string;

    @ApiProperty({type: ShowUsersDto})
    @Publish(ShowUsersDto)
    public commentedBy: UserEntity;

    @ApiProperty()
    @Publish()
    public createdOn: Date;
}
