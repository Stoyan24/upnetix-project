import {ApiProperty} from '@nestjs/swagger';
import {IsNotEmpty, IsString, MaxLength, MinLength} from 'class-validator';

export class UpdateCommentDto {

    @ApiProperty({
        type: String,
        minLength: 15,
        maxLength: 400
    })
    @IsString()
    @IsNotEmpty()
    @MinLength(15, {
        message: 'Content is too short. Minimal length is $constraint1 characters, but actual is $value'
    })
    @MaxLength(400, {
        message: 'Content is too long. Maximal length is $constraint1 characters, but actual is $value'
    })
    public commentContent: string;

}
