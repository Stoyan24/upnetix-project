import {
    Body,
    Controller,
    Delete,
    HttpCode,
    HttpStatus,
    Param,
    Post,
    Put,
    UseGuards,
    UseInterceptors, ValidationPipe,
} from '@nestjs/common';
import {AuthGuard} from '@nestjs/passport';
import {ApiBearerAuth, ApiCreatedResponse, ApiNotFoundResponse, ApiOkResponse, ApiTags} from '@nestjs/swagger';
import {UserRoleType} from '../common/enums/user-role';
import {Roles} from '../common/middleware/decorators/roles-decorator';
import {USER} from '../common/middleware/decorators/user-decorator';
import {RoleGuard} from '../common/middleware/guards/admin.guard';
import {TransformInterceptor} from '../common/middleware/transformer/interceptors/transform.interceptor';
import {CommentIdValidation} from '../common/pipes/custom-validators/commentId.validation';
import {PostIdValidation} from '../common/pipes/custom-validators/postId.validation';
import {CommentEntity} from '../database/entities/comment.entity';
import {UserEntity} from '../database/entities/user.entity';
import {CommentsService} from './comments.service';
import {CreateCommentDto} from './models/create-comment.dto';
import {ShowPostCommentsDto} from './models/show-post-comments.dto';
import {UpdateCommentDto} from './models/update-comment.dto';

@ApiTags('unpetix/comments')
@Controller('upnetix/comments')
export class CommentsController {
    constructor(private readonly commentsService: CommentsService) {
    }

    @Post(':postId')
    @ApiBearerAuth()
    @ApiCreatedResponse({description: 'Creates comment', type: ShowPostCommentsDto})
    @ApiNotFoundResponse({description: 'Post with such id not found'})
    @UseInterceptors(new TransformInterceptor(ShowPostCommentsDto))
    @Roles(UserRoleType.User)
    @UseGuards(AuthGuard('jwt'), RoleGuard)
    @HttpCode(HttpStatus.OK)
    public async createComment(
        @Param(new ValidationPipe({whitelist: true, transform: true, errorHttpStatusCode: 404})) postId: PostIdValidation,
        @USER() user: UserEntity,
        @Body(new ValidationPipe({whitelist: true, transform: true})) body: CreateCommentDto
    ): Promise<CommentEntity> {
        return await this.commentsService.createComment(postId, body, user)
    }

    @Put(':commentId')
    @ApiBearerAuth()
    @ApiOkResponse({status: 201, description: 'Updates comment', type: ShowPostCommentsDto})
    @ApiNotFoundResponse({description: 'Comment with such id not found'})
    @UseInterceptors(new TransformInterceptor(ShowPostCommentsDto))
    @Roles(UserRoleType.User)
    @UseGuards(AuthGuard('jwt'), RoleGuard)
    @HttpCode(HttpStatus.OK)
    public async editComment(
        @Param(new ValidationPipe({whitelist: true, transform: true, errorHttpStatusCode: 404})) commentId: CommentIdValidation,
        @USER() user: UserEntity,
        @Body(new ValidationPipe({whitelist: true, transform: true})) body: UpdateCommentDto
    ): Promise<CommentEntity> {
        return await this.commentsService.editComment(commentId, user, body)
    }

    @Delete(':commentId')
    @ApiBearerAuth()
    @ApiOkResponse({status: 201, description: 'Deletes comment'})
    @ApiNotFoundResponse({description: 'Comment with such id not found'})
    @Roles(UserRoleType.User)
    @UseGuards(AuthGuard('jwt'), RoleGuard)
    @HttpCode(HttpStatus.OK)
    public async deleteComment(@Param(new ValidationPipe({whitelist: true, transform: true, errorHttpStatusCode: 404})) commentId: CommentIdValidation, @USER() user: UserEntity): Promise<{commentId: string}> {
        return await this.commentsService.deleteComment(commentId, user);
    }

}
