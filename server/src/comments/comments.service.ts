import {HttpStatus, Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {Repository} from 'typeorm';
import {NotificationType} from '../common/enums/notification-type';
import {UpnetixSystemError} from '../common/middleware/exceptions/upnetix-system.error';
import {CommentIdValidation} from '../common/pipes/custom-validators/commentId.validation';
import {PostIdValidation} from '../common/pipes/custom-validators/postId.validation';
import {CommentEntity} from '../database/entities/comment.entity';
import {PostEntity} from '../database/entities/post.entity';
import {RoleEntity} from '../database/entities/role.entity';
import {UserEntity} from '../database/entities/user.entity';
import {SocketGateway} from '../gateways/socket.gateway';
import {HelperService} from '../helper/helper.service';
import {NotificationsService} from '../notifications/notifications.service';
import {CreateCommentDto} from './models/create-comment.dto';
import {UpdateCommentDto} from './models/update-comment.dto';

@Injectable()
export class CommentsService {

    public constructor(
        @InjectRepository(UserEntity) private readonly userRepository: Repository<UserEntity>,
        @InjectRepository(RoleEntity) private readonly roleRepository: Repository<RoleEntity>,
        @InjectRepository(PostEntity) private readonly postRepository: Repository<PostEntity>,
        @InjectRepository(CommentEntity) private readonly commentRepository: Repository<CommentEntity>,
        private readonly helperService: HelperService,
        private readonly socketGateway: SocketGateway,
        private readonly notificationService: NotificationsService,
    ) {
    }

    public async createComment({postId}: PostIdValidation, body: CreateCommentDto, user: UserEntity): Promise<CommentEntity> {
        let post: PostEntity;
        let foundUser: UserEntity;
        try {
            post = await this.postRepository.findOne({where: {postId}, relations: ['comments']});
            foundUser = await this.userRepository.findOne({where: {userId: user.userId}, relations: ['userCommends']})
        } catch (err) {
            throw new UpnetixSystemError(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
        }
        if (!foundUser) {
            throw new UpnetixSystemError('User with such id not found!', HttpStatus.BAD_REQUEST);
        }
        delete (user as any).__subscribers__;
        delete (user as any).__subscriptions__;
        const newComment: CommentEntity = this.commentRepository.create();
        newComment.commentContent = body.commentContent;
        newComment.commentedPost = Promise.resolve({...post, __comments__: []});
        newComment.commentedBy = user;

        const savedComment = await this.commentRepository.save(newComment);
        (post as any).__comments__ = [...(post as any).__comments__, savedComment];
        post.commentsCount++;
        await this.postRepository.save(post);
        (foundUser as any).__userCommends__ = [...(foundUser as any).__userCommends__, savedComment];
        await this.userRepository.save(foundUser);
        const newNotification = await this.notificationService.createNotification(
            user,
            (post as any).__postedBy__.userId,
            ' commented your post',
            NotificationType.POST,
            {id: post.postId, imgUrl: post.photoUrl},
        );
        await this.socketGateway.notify((post as any).__postedBy__.userName, newNotification);
        return savedComment;
    }

    public async editComment({commentId}: CommentIdValidation, user: UserEntity, body: UpdateCommentDto): Promise<CommentEntity> {
        let comment: CommentEntity;
        try {
            comment = await this.commentRepository.findOne(commentId);
        } catch (err) {
            throw new UpnetixSystemError(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
        }
        const checkOwner = comment.commentedBy.userId === user.userId;
        if (checkOwner) {
            comment.commentContent = body.commentContent;
        } else if (this.helperService.isAdmin(user.roles)) {
            comment.commentContent = body.commentContent;
        } else {
            throw new UpnetixSystemError('Only the owner of the comment can update it!', HttpStatus.BAD_REQUEST)
        }

        return await this.commentRepository.save(comment);
    }

    async deleteComment({commentId}: CommentIdValidation, user: UserEntity): Promise<{ commentId: string }> {
        let comment: CommentEntity;
        let targetPost: PostEntity;
        let targetUser: UserEntity;
        try {
            comment = await this.commentRepository.findOne({where: {commentId}, relations: ['commentedPost']});
        } catch (err) {
            throw new UpnetixSystemError(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
        }
        const checkOwner: boolean = comment.commentedBy.userId === user.userId;
        if (checkOwner) {
            await this.commentRepository.delete(commentId);
        } else if (this.helperService.isAdmin(user.roles)) {
            await this.commentRepository.delete(commentId);
        } else {
            throw new UpnetixSystemError('Only the owner of the comment can update it!', HttpStatus.BAD_REQUEST)
        }

        try {
            await this.commentRepository.delete(commentId);
        } catch (err) {
            throw new UpnetixSystemError(err.message, HttpStatus.INTERNAL_SERVER_ERROR)
        }
        const postId: string = (comment as any).__commentedPost__.postId;
        // update post comments
        targetPost = await this.postRepository.findOne({where: {postId}, relations: ['comments']});
        (targetPost as any).__comments__ = (targetPost as any).__comments__.filter((c: CommentEntity) => c.commentId !== comment.commentId);
        targetPost.commentsCount--;
        await this.postRepository.save(targetPost);

        // update user comments
        targetUser = await this.userRepository.findOne({where: {userId: user.userId}, relations: ['userCommends']});
        (targetUser as any).__userCommends__ = (targetUser as any).__userCommends__.filter((c: CommentEntity) => c.commentId !== comment.commentId);
        await this.userRepository.save(targetUser);
        return {commentId};
    }
}
