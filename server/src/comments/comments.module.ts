import { Module } from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {IsCommentIdValidConstraint} from '../common/pipes/custom-class-validator/IsCommentIdValid';
import {IsPostIdValidConstraint} from '../common/pipes/custom-class-validator/IsPostIdValid';
import {CommentEntity} from '../database/entities/comment.entity';
import {NotificationEntity} from '../database/entities/notification.entity';
import {PostEntity} from '../database/entities/post.entity';
import {RoleEntity} from '../database/entities/role.entity';
import {UserEntity} from '../database/entities/user.entity';
import {SocketModule} from '../gateways/socket.module';
import {NotificationsService} from '../notifications/notifications.service';
import { CommentsController } from './comments.controller';
import { CommentsService } from './comments.service';

@Module({
  imports: [
      TypeOrmModule.forFeature([
          UserEntity,
          PostEntity,
          CommentEntity,
          RoleEntity,
          NotificationEntity,
      ]),
      SocketModule,
  ],
  controllers: [CommentsController],
  providers: [
      CommentsService,
      IsPostIdValidConstraint,
      IsCommentIdValidConstraint,
      NotificationsService
  ],
  exports: [CommentsService]
})
export class CommentsModule {}
