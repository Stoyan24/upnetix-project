import {HttpStatus, Injectable} from '@nestjs/common';
import {PassportStrategy} from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import {UpnetixSystemError} from '../../common/middleware/exceptions/upnetix-system.error';
import {ConfigService} from '../../config/config.service';
import {UsersService} from '../../users/users.service';
import {IPayload} from '../models/IPayload';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly userService: UsersService,
    configService: ConfigService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: configService.jwtSecret,
      ignoreExpiration: false,
    });
  }

  public async validate(payload: IPayload): Promise<any> {
    const user = await this.userService.findUserByUsername(
      payload.userName,
    );

    if (!user) {
      throw new UpnetixSystemError('Unauthorized', HttpStatus.UNAUTHORIZED);
    }

    return user;
  }
}
