import {HttpStatus, Injectable} from '@nestjs/common';
import {JwtService} from '@nestjs/jwt';
import {UpnetixSystemError} from '../common/middleware/exceptions/upnetix-system.error';
import {RoleEntity} from '../database/entities/role.entity';
import {UserEntity} from '../database/entities/user.entity';
import {UsersService} from '../users/users.service';
import {IPayload} from './models/IPayload';
import {LoginDto} from './models/login.dto';

@Injectable()
export class AuthService {

    public constructor(
        private readonly userService: UsersService,
        private readonly jwtService: JwtService,
    ) {
    }

    public async login(user: LoginDto): Promise<any> {
        const foundUser: UserEntity = await this.userService.findUserByUsername(
            user.usernameOrEmail,
        );

        if (!foundUser) {
            throw new UpnetixSystemError('Unauthorized', HttpStatus.UNAUTHORIZED);
        }
        if (!(await this.userService.validateUserPassword(user))) {
            throw new UpnetixSystemError('Invalid credentials!', HttpStatus.BAD_REQUEST);
        }
        const payload: IPayload = {
            userName: foundUser.userName,
            userId: foundUser.userId,
            email: foundUser.email,
            roles: foundUser.roles.map((role: RoleEntity) => role.roleType),
            userAvatarUrl: foundUser.userAvatarUrl
        };
        return {
            token: await this.jwtService.signAsync(payload),
        };
    }
}
