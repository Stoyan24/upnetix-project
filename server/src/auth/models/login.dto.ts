import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, Length, Matches } from 'class-validator';

export class LoginDto {
    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    @Length(3, 50)
    public usernameOrEmail: string;

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    @Matches(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/, {
        message:
            'The password must be minimum eight characters, one letter and one number',
    })
    public password: string;
}
