import {UserRoleType} from '../../common/enums/user-role';

export interface IPayload {
    userName: string;
    userId: string;
    email: string;
    roles: UserRoleType[];
    userAvatarUrl: string;
}
