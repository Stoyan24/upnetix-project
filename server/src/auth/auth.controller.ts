import {Body, Controller, Delete, HttpCode, HttpStatus, Post, ValidationPipe} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiTags
} from '@nestjs/swagger';
import { AuthService } from './auth.service';
import { LoginDto } from './models/login.dto';

@ApiTags('session')
@Controller('upnetix/session')
export class AuthController {
  public constructor(private readonly authService: AuthService) {}

  @Post()
  @HttpCode(HttpStatus.OK)
  @ApiCreatedResponse({description: 'Access granted!', type: LoginDto})
  @ApiBadRequestResponse({description: 'Access denied!'})
  public async loginUser(@Body(new ValidationPipe({whitelist: true, transform: true})) user: LoginDto): Promise<{ token: string }> {
    return await this.authService.login(user);
  }

  @Delete()
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({description: 'Logged out!'})
  @ApiBadRequestResponse({description: 'Cannot be logged out!'})
  @ApiBearerAuth()
  public async logoutUser() {
    return true;
  }
}
