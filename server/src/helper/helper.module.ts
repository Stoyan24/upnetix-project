import { Module } from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {PostEntity} from '../database/entities/post.entity';
import {RoleEntity} from '../database/entities/role.entity';
import {UserEntity} from '../database/entities/user.entity';
import { HelperService } from './helper.service';
import {MapperService} from './mapper.service';

@Module({
  imports: [ TypeOrmModule.forFeature([
    RoleEntity,
    UserEntity,
    PostEntity,
  ])],
  providers: [HelperService, MapperService],
  exports: [HelperService, MapperService]
})
export class HelperModule {}
