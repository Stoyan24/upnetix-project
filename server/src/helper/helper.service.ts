import { Injectable } from '@nestjs/common';
import {UserRoleType} from '../common/enums/user-role';
import {RoleEntity} from '../database/entities/role.entity';
import {UserEntity} from '../database/entities/user.entity';

@Injectable()
export class HelperService {

    public isFollower(user: UserEntity, userId) {
        return (user as any).__subscriptions__.some((u: UserEntity) => u.userId === userId);
    }

    public isAdmin(roles: RoleEntity[]): boolean {
        return !!roles.find((role: RoleEntity) => role.roleType === UserRoleType.Admin);
    }
}
