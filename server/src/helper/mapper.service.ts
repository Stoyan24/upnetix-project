import { Injectable } from '@nestjs/common';
import {plainToClass} from 'class-transformer';
import {NotificationEntity} from '../database/entities/notification.entity';
import {PostEntity} from '../database/entities/post.entity';
import {ShowNotificationDto} from '../notifications/models/show-notification.dto';
import {ShowPostsDto} from '../posts/models/show-posts.dto';

@Injectable()
export class MapperService {

    public toPostsDto(post: PostEntity): ShowPostsDto {
        return plainToClass(ShowPostsDto, post, {excludeExtraneousValues: true, })
    }

    public mapNotificationDto(notification: NotificationEntity | NotificationEntity[]): ShowNotificationDto{
        return plainToClass(ShowNotificationDto, notification, {excludeExtraneousValues: true})
    }
}
