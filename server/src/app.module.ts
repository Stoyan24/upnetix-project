import {Module} from '@nestjs/common';
import {ScheduleModule} from '@nestjs/schedule';
import {TaskSchedulingModule} from './Task-Scheduling/task-scheduling.module';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {CommentsModule} from './comments/comments.module';
import {CoreModule} from './common/core.module';
import {DatabaseModule} from './database/database.module';
import {NotificationsModule} from './notifications/notifications.module';
import {PostsModule} from './posts/posts.module';
import {UsersModule} from './users/users.module';

@Module({
    imports: [
        DatabaseModule,
        CoreModule,
        UsersModule,
        PostsModule,
        CommentsModule,
        NotificationsModule,
        ScheduleModule.forRoot(),
        TaskSchedulingModule,
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {}
