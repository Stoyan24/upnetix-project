import {Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn} from 'typeorm';
import {NotificationType} from '../../common/enums/notification-type';
import {UserEntity} from './user.entity';

@Entity('notifications')
export class NotificationEntity {
    @PrimaryGeneratedColumn('uuid')
    public notificationId: string;

    @Column({type: 'json'})
    public notifiedBy: Partial<UserEntity>;

    @Column({type: 'nvarchar', length: 200})
    public message: string;

    @Column({type: 'json'})
    public optionalInfo: {id: string, imgUrl: string};

    @ManyToOne(type => UserEntity, user => user.notifications, {eager: true, cascade: true})
    to: Promise<UserEntity>;

    @Column({ type: 'boolean', default: false })
    read: boolean;

    @Column({type: 'enum', enum: NotificationType})
    public type: NotificationType;

    @CreateDateColumn()
    createdOn: Date;

    @UpdateDateColumn()
    updatedOn: Date;

}
