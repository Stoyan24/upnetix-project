import {Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn} from 'typeorm';
import {PostEntity} from './post.entity';
import {UserEntity} from './user.entity';

@Entity('comments')
export class CommentEntity {
    @PrimaryGeneratedColumn('uuid')
    public commentId: string;

    @Column({type: 'nvarchar', length: 400, nullable: false})
    public commentContent: string;

    @ManyToOne(type => PostEntity, post => post.comments)
    public commentedPost: Promise<PostEntity>;

    @ManyToOne(type => UserEntity, user => user.userCommends, {eager: true})
    public commentedBy: UserEntity;

    @CreateDateColumn()
    public createdOn: Date;
}
