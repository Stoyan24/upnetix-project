import {Column, Entity, PrimaryGeneratedColumn} from 'typeorm';
import {UserRoleType} from '../../common/enums/user-role';

@Entity('roles')
export class RoleEntity {
    @PrimaryGeneratedColumn('uuid')
    public roleId: string;

    @Column({type: 'enum', enum: UserRoleType})
    public roleType: UserRoleType;
}
