import {Column, CreateDateColumn, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn} from 'typeorm';
import {PostStatus} from '../../common/enums/post-status';
import {CommentEntity} from './comment.entity';
import {UserEntity} from './user.entity';
import {LocationInterface} from "../../posts/models/location.interface";

@Entity('posts')
export class PostEntity {
    @PrimaryGeneratedColumn('uuid')
    public postId: string;

    @ManyToOne(type => UserEntity, user => user.posts, {eager: true})
    public postedBy: Promise<UserEntity>;

    @Column({type: 'int', default: 0})
    public likes: number;

    @Column({type: 'int', default: 0})
    public commentsCount: number;

    @Column({type: 'enum', default: PostStatus.PRIVATE, enum: PostStatus})
    public status: PostStatus;

    @OneToMany(type => CommentEntity, comment => comment.commentedPost)
    public comments: Promise<CommentEntity[]>;

    @Column({type: 'boolean', default: false})
    public isDeleted: boolean;

    @Column({type: 'nvarchar', length: 40, nullable: false})
    public title: string;

    @Column({type: 'nvarchar', length: 200, nullable: false})
    public description: string;

    @Column({type: 'nvarchar'})
    public photoUrl: string;

    @Column({type: 'json'})
    public location: LocationInterface;

    @Column({type: 'boolean', default: false})
    public isLiked: boolean;

    @CreateDateColumn({type: 'timestamp'})
    public createdOn: Date;
}
