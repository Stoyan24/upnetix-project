import {Column, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn} from 'typeorm';
import {CommentEntity} from './comment.entity';
import {NotificationEntity} from './notification.entity';
import {PostEntity} from './post.entity';
import {RoleEntity} from './role.entity';

@Entity('users')
export class UserEntity {
    @PrimaryGeneratedColumn('uuid')
    public userId: string;

    @Column({type: 'nvarchar', nullable: false, unique: true, length: 20}) // Min lenth
    public userName: string;

    @Column({ type: 'nvarchar', length: 50, unique: true, nullable: false})
    public email: string;

    @Column({type: 'nvarchar', nullable: false})
    public password: string;

    @Column({type: 'nvarchar', length: 200})
    public userBio: string;

    @Column({type: 'nvarchar'})
    public userAvatarUrl: string;

    @ManyToMany(type => UserEntity, user => user.subscriptions)
    @JoinTable()
    subscribers: Promise<UserEntity[]>;

    @ManyToMany(type => UserEntity)
    @JoinTable()
    subscriptions: Promise<UserEntity[]>;

    @ManyToMany(type => RoleEntity, {eager: true})
    @JoinTable()
    public roles: RoleEntity[];

    @ManyToMany(type => PostEntity)
    @JoinTable()
    public likedPosts: Promise<PostEntity[]>;

    @OneToMany(type => PostEntity, post => post.postedBy, {onDelete: 'CASCADE'})
    public posts: Promise<PostEntity[]>;

    @OneToMany(type => NotificationEntity, notification => notification.to)
    public notifications: Promise<NotificationEntity[]>;

    @OneToMany(type => CommentEntity, comment => comment.commentedBy)
    public userCommends: Promise<CommentEntity[]>

}
