import { ConnectionOptions } from 'typeorm';

const config: ConnectionOptions = {
    type: 'mysql',
    host: 'localhost',
    port: 3306,
    username: 'root',
    password: 'ribamech',
    database: 'upnetix',
    entities: [__dirname + '/**/*.entity{.ts,.js}'],

    synchronize: true,

    migrationsRun: true,

    migrations: [__dirname + '/**/migrations/**/*{.ts,.js}'],
    cli: {
        migrationsDir: 'src/database/migrations',
    },
};

export = config;
