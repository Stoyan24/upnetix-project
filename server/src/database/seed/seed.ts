import * as bcrypt from 'bcrypt';
import {createConnection} from 'typeorm';
import {UserRoleType} from '../../common/enums/user-role';
import {RoleEntity} from '../entities/role.entity';
import {UserEntity} from '../entities/user.entity';

const seed = async () => {

    const connection = await createConnection();
    const userRepository = connection.getRepository(UserEntity);
    const roleRepository = connection.getRepository(RoleEntity);

    // Creating user roles
    const admin = await roleRepository.save({
        roleType: UserRoleType.Admin,
    });
    const user = await roleRepository.save({
        roleType: UserRoleType.User,
    });

    const firstAdmin = new UserEntity();
    firstAdmin.userAvatarUrl = 'https://www.satanbeer.com/heks2.gif';
    firstAdmin.email = 'admin@abv.com';
    firstAdmin.userName = 'admin';
    firstAdmin.userCommends = Promise.resolve([]);
    firstAdmin.posts = Promise.resolve([]);
    firstAdmin.likedPosts = Promise.resolve([]);
    firstAdmin.userBio = 'Best admin and I will catch and punish you all !!?';
    firstAdmin.subscriptions = Promise.resolve([]);
    firstAdmin.subscribers = Promise.resolve([]);
    firstAdmin.roles = [admin, user];
    firstAdmin.password = await bcrypt.hash('123456789a', 10);

    await userRepository.save(firstAdmin);

    const gosho = new UserEntity();
    gosho.userAvatarUrl = 'https://media.wired.com/photos/592660257034dc5f91beb31a/2:1/w_2500,c_limit/Thor.jpg';
    gosho.email = 'gosho@abv.com';
    gosho.userName = 'gosho';
    gosho.userCommends = Promise.resolve([]);
    gosho.posts = Promise.resolve([]);
    gosho.likedPosts = Promise.resolve([]);
    gosho.userBio = 'Best admin and I will catch and punish you all !!?';
    gosho.subscriptions = Promise.resolve([]);
    gosho.subscribers = Promise.resolve([]);
    gosho.roles = [admin, user];
    gosho.password = await bcrypt.hash('123456789a', 10);

    await userRepository.save(gosho);

    const pesho = new UserEntity();
    pesho.userAvatarUrl = 'https://i.imgur.com/7brAon0.png';
    pesho.email = 'pesho@abv.com';
    pesho.userName = 'pesho';
    pesho.userCommends = Promise.resolve([]);
    pesho.posts = Promise.resolve([]);
    pesho.likedPosts = Promise.resolve([]);
    pesho.userBio = 'Best admin and I will catch and punish you all !!?';
    pesho.subscriptions = Promise.resolve([]);
    pesho.subscribers = Promise.resolve([]);
    pesho.roles = [admin, user];
    pesho.password = await bcrypt.hash('123456789a', 10);

    await userRepository.save(pesho);

    await connection.close();

    console.log(`Data seeded successfully`);
};
seed()
    .catch(console.log);
