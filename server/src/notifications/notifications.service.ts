import {HttpStatus, Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {Repository} from 'typeorm';
import {NotificationType} from '../common/enums/notification-type';
import {UpnetixSystemError} from '../common/middleware/exceptions/upnetix-system.error';
import {UserIdValidation} from '../common/pipes/custom-validators/userId.validation';
import {NotificationEntity} from '../database/entities/notification.entity';
import {UserEntity} from '../database/entities/user.entity';

@Injectable()
export class NotificationsService {

    public constructor(
        @InjectRepository(UserEntity) private readonly userRepository: Repository<UserEntity>,
        @InjectRepository(NotificationEntity) private readonly notificationRepository: Repository<NotificationEntity>,
    ) {
    }

    public async createNotification(
        notifyingUser: Partial<UserEntity>,
        notifiedUserId: string,
        message: string,
        type: NotificationType,
        optInfo: { id: string, imgUrl: string } = {id: 'EMPTY', imgUrl: 'EMPTY'}
    ): Promise<NotificationEntity> {
        const foundUser = await this.userRepository.findOne({
            where: {userId: notifiedUserId}
        });
        if (!foundUser) {
            throw new UpnetixSystemError('User not found', HttpStatus.INTERNAL_SERVER_ERROR)
        }
        if (notifyingUser.userId === notifiedUserId) {
            return null
        }
        const notification: NotificationEntity = this.notificationRepository.create();
        notification.notifiedBy = {
            userName: notifyingUser.userName,
            userAvatarUrl: notifyingUser.userAvatarUrl,
            userId: notifyingUser.userId,
        };
        notification.type = type;
        notification.optionalInfo = optInfo;
        notification.message = message;
        notification.to = Promise.resolve(foundUser);

        return await this.notificationRepository.save(notification);
    }

    public async allNotifications(): Promise<NotificationEntity[]> {
        return await this.notificationRepository.find();
    }

    public async getUserNotifications(userId: UserIdValidation): Promise<NotificationEntity[]> {
        return await this.notificationRepository
            .createQueryBuilder('notifications')
            .leftJoinAndSelect('notifications.to', 'user')
            .leftJoinAndSelect('user.roles', 'roles')
            .where('user.userId = :userId', userId)
            .orderBy('notifications.createdOn', 'DESC')
            .getMany();
    }

    public async updateNotification(notificationId: string, user: UserEntity): Promise<NotificationEntity> {
        const notification = await this.notificationRepository.findOne(notificationId);
        if (!notification) {
            throw new UpnetixSystemError('Notification with this id not found!', HttpStatus.BAD_REQUEST)
        }
        if ((notification as any).__to__.userId !== user.userId) {
            throw new UpnetixSystemError('You cant update other users notifications!', HttpStatus.FORBIDDEN)
        }
        notification.read = true;
        return await this.notificationRepository.save(notification);
    }
}
