import {
    Controller,
    Get,
    HttpCode,
    HttpStatus,
    Param,
    Put,
    UseGuards,
    UseInterceptors,
    ValidationPipe
} from '@nestjs/common';
import {AuthGuard} from '@nestjs/passport';
import {ApiOkResponse} from '@nestjs/swagger';
import {UserRoleType} from '../common/enums/user-role';
import {Roles} from '../common/middleware/decorators/roles-decorator';
import {USER} from '../common/middleware/decorators/user-decorator';
import {RoleGuard} from '../common/middleware/guards/admin.guard';
import {TransformInterceptor} from '../common/middleware/transformer/interceptors/transform.interceptor';
import {UserIdValidation} from '../common/pipes/custom-validators/userId.validation';
import {NotificationEntity} from '../database/entities/notification.entity';
import {UserEntity} from '../database/entities/user.entity';
import {ShowNotificationDto} from './models/show-notification.dto';
import {NotificationsService} from './notifications.service';

@Controller('upnetix/notifications')
export class NotificationsController {

    constructor(private readonly notificationsService: NotificationsService) {
    }

    @Get()
    @Roles(UserRoleType.User)
    @UseGuards(AuthGuard('jwt'), RoleGuard)
    @ApiOkResponse({status: 201, description: 'Returns all users notification', type: ShowNotificationDto})
    @UseInterceptors(new TransformInterceptor(ShowNotificationDto))
    @HttpCode(HttpStatus.OK)
    public async getAllNotification(): Promise<NotificationEntity[]> {
        return await this.notificationsService.allNotifications()
    }

    @Get(':userId/notifications')
    @Roles(UserRoleType.User)
    @UseGuards(AuthGuard('jwt'), RoleGuard)
    @ApiOkResponse({status: 201, description: 'Returns user notifications', type: ShowNotificationDto})
    @UseInterceptors(new TransformInterceptor(ShowNotificationDto))
    @HttpCode(HttpStatus.OK)
    public async getUserNotifications(@Param(new ValidationPipe({
        whitelist: true,
        transform: true,
        errorHttpStatusCode: 404
    })) userId: UserIdValidation): Promise<NotificationEntity[]> {
        return await this.notificationsService.getUserNotifications(userId)
    }

    @Put(':notificationId')
    @Roles(UserRoleType.User)
    @UseGuards(AuthGuard('jwt'), RoleGuard)
    @ApiOkResponse({status: 201, description: 'Returns updated notification', type: ShowNotificationDto})
    @UseInterceptors(new TransformInterceptor(ShowNotificationDto))
    @HttpCode(HttpStatus.OK)
    public async updateNotification(@Param() notificationId: string, @USER() user: UserEntity): Promise<NotificationEntity> {
        return await this.notificationsService.updateNotification(notificationId, user);
    }
}
