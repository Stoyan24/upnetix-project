import { Module } from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {NotificationEntity} from '../database/entities/notification.entity';
import {UserEntity} from '../database/entities/user.entity';
import { NotificationsController } from './notifications.controller';
import { NotificationsService } from './notifications.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      UserEntity,
      NotificationEntity,
    ])
  ],
  controllers: [NotificationsController],
  providers: [NotificationsService],
  exports: [NotificationsService]
})
export class NotificationsModule {}
