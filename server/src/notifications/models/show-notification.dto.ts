import {ApiProperty} from '@nestjs/swagger';
import {Expose, Type} from 'class-transformer';
import {NotificationType} from '../../common/enums/notification-type';
import {Publish} from '../../common/middleware/transformer/decorators/publish';
import {UserEntity} from '../../database/entities/user.entity';
import {ShowUsersDto} from '../../users/models/show-users.dto';

export class ShowNotificationDto {

    @ApiProperty()
    @Publish()
    @Expose()
    public notificationId: string;

    @ApiProperty({type: ShowUsersDto})
    @Expose({name: '__to__'})
    @Type(() => ShowUsersDto)
    @Publish(ShowUsersDto)
    public to: UserEntity;

    @ApiProperty()
    @Publish()
    @Expose()
    public message: string;

    @ApiProperty()
    @Publish()
    @Expose()
    public notifiedBy: Partial<UserEntity>;

    @ApiProperty()
    @Publish()
    @Expose()
    public read: boolean;

    @ApiProperty()
    @Publish()
    @Expose()
    public type: NotificationType;

    @ApiProperty()
    @Publish()
    @Expose()
    public optionalInfo: {id: string, imgUrl: string};

    @ApiProperty()
    @Publish()
    @Expose()
    public createdOn: Date;

    @ApiProperty()
    @Publish()
    @Expose()
    public updatedOn: Date;
}
