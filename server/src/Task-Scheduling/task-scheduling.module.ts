import { Module } from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {NotificationEntity} from '../database/entities/notification.entity';
import { TaskSchedulingService } from './task-scheduling.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      NotificationEntity,
    ])
  ],
  providers: [TaskSchedulingService]
})
export class TaskSchedulingModule {}
