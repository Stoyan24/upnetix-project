import {Injectable, Logger} from '@nestjs/common';
import {Interval} from '@nestjs/schedule';
import {InjectRepository} from '@nestjs/typeorm';
import {LessThan, Repository} from 'typeorm';
import {NotificationEntity} from '../database/entities/notification.entity';

@Injectable()
export class TaskSchedulingService {
    private readonly logger = new Logger(TaskSchedulingService.name);

    constructor(@InjectRepository(NotificationEntity) private readonly notificationRepository: Repository<NotificationEntity>) {
    }

    @Interval(259200)
    async handleInterval() {
        const date = new Date();
        const lastWeekDate = new Date();
        lastWeekDate.setDate(date.getDate() - 3);
        const deletedResult = await this.notificationRepository.delete({read: true, createdOn: LessThan(lastWeekDate)});
        this.logger.log('Number of deleted notifications - ' + deletedResult.affected);
    }
}
