import {ApiProperty} from '@nestjs/swagger';
import {UserRoleType} from '../../common/enums/user-role';
import {Publish} from '../../common/middleware/transformer/decorators/publish';

export class ShowRolesDto {
    @ApiProperty()
    @Publish()
    public roleType: UserRoleType;
}
